import React, { useState, useEffect} from 'react'
import axios from 'axios'

const FormSic = ({onClose=()=>{}}) => {
  const [loaded, setLoaded] = useState(false)
  const [loading, setLoading] = useState(false)
  const [nome_solicitante, setNome] = useState('')
  const [email_solicitante, setEmail] = useState('')
  const [area_solicitacao, setArea] = useState('')
  const [assunto, setAssunto] = useState('')
  const [mensagem, setMsg] = useState('')
  const [error, setError] = useState(false)
  const [errorMsg, setErrorMsg] = useState("Favor preencher todos os campos!!!")
  const [success, setSuccess] = useState(false)
  const [successMsg, setSuccessMsg] = useState("")
  const [protocolo, setProtocolo] = useState("")
  
  const required = ["nome_solicitante", "email_solicitante", "area_solicitacao", "assunto", "mensagem"]

  useEffect(() => {
    if(!loaded){

    }
  },[])
  const send = () => {
    setSuccess(false)
    setSuccessMsg('')
    setLoading(true)
    const data = {
      nome_solicitante,
      email_solicitante,
      area_solicitacao,
      assunto,
      mensagem
    }
    console.log(data)
    required.forEach(r=>{

      if(!data[r] || data[r]==''){
        
        setError(true)
        setLoading(false)
      }
    })
    if(!error){
      axios.post('/api/sic/nova-solicitacao',data)
        .then(r=>{
          console.log(r.data.success)
          if(r.data.success){
            setSuccess(true)
            setProtocolo(r.data.data[0].protocolo)
            setLoading(false)
            
          }else{
            setError(true)
            setErrorMsg(r.data.msg)
            setLoading(false)
          }
        })
    }
    setTimeout(()=>{
      setError(false)
      setLoading(false)
    },3000)
  }

  const toggleStyles = (event) => {
      document.querySelector('#modal-sic').classList.toggle('is-active')
      setArea('')
      setAssunto('')
      setNome('')
      setEmail('')
      setMsg('')
      setSuccess(false)
      setSuccessMsg('')
  }
  const close = (event) => {
      toggleStyles()
      onClose()
  }
  var ld = loading?"is-loading is-disabled":""
  return (
    <>
    <buton class="button is-primary is-outlined" onClick={toggleStyles}>Nova Solicitação</buton>
    <div class="modal" id="modal-sic">
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title">Nova Solicitação</p>
          <button class="delete" aria-label="close" onClick={close}></button>
        </header>
        <section class="modal-card-body">
          
          <div class="field">
            <label class="label">Nome</label>
            <div class="control has-icons-left">
              <input class="input" type="text" placeholder="Informe seu Nome" value={nome_solicitante} onChange={(e)=>{setNome(e.target.value)}}/>
              <span class="icon is-small is-left">
                <i class="fas fa-user"></i>
              </span>
            </div>
          </div>

          <div class="field">
            <label class="label">Email</label>
            <div class="control has-icons-left">
              <input class="input" type="text" placeholder="Informe um e-mail válido" value={email_solicitante} onChange={(e)=>{setEmail(e.target.value)}}/>
              <span class="icon is-small is-left">
                <i class="fas fa-envelope"></i>
              </span>
            </div>
            <p class="help is-danger"><b>Aviso!</b> Informe um e-mail válido, pois a resposta será enviada para ele!</p>
          </div>
          
          <div class="field">
            <label class="label">Assunto</label>
            <div class="control has-icons-left">
              <input class="input" type="text" placeholder="Informe o assunto da sua solicitação" value={assunto} onChange={(e)=>{setAssunto(e.target.value)}}/>
              <span class="icon is-small is-left">
                <i class="fas fa-file"></i>
              </span>
            </div>
          </div>
          
          <div class="field">
            <label class="label">Área de interesse</label>
            <div class="control">
              <div class="select">
                <select value={area_solicitacao} onChange={(e)=>{setArea(e.target.value)}}>
                  <option value="" selected readOnly >Selecione uma área</option>
                  <option>Ouvidoria</option>
                  <option>Administração</option>
                  <option>Assessoria Legislativa e Jurídica</option>
                  <option>Comissões</option>
                  <option>Secretaria Legislativa</option>
                  <option>Plenário</option>
                </select>
              </div>
            </div>
          </div>
          
          <div class="field">
            <label class="label">Solicitação</label>
            <div class="control">
              <textarea class="textarea" value={mensagem} onChange={(e)=>{setMsg(e.target.value)}} placeholder="Digite aqui sua solicitação de informação."></textarea>
            </div>
          </div>
          {
            error?
            <div class={"notification is-danger "}>
              {errorMsg}
            </div>
            :''
          }
          
          {
            success?
            <div class={"notification is-success "}>
              <b>Solicitação enviada com sucesso!</b>
              <br/>
              Protocolo: {protocolo}
              <br/>
              <b>Este número servirá para consultar sua solicitação.</b>
            </div>
            :''
          }
          
        </section>
        <footer class="modal-card-foot">
          <button disabled={success} className={"button is-primary "+ld} onClick={()=>send()}>Enviar</button>
          <button class="button is-danger is-outlined" onClick={close}>Fechar</button>
        </footer>
      </div>
    </div>
      
    </>
  )
}
export default FormSic