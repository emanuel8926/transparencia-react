import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { NextSeo } from 'next-seo'
import config from '../config.json'
import Breadcrumb from './breadcrumb'

const PageHero = (props) => {
    

    return (
        <>
            <NextSeo
                title={props.title+' - '+config.siteMeta.title}
                description={props.subtitle}
            />
            <div >
            <Breadcrumb links={props.breadcrumb} />
            <div class="columns">
                <div class="column is-narrow has-text-left">
                    {
                        props.photo?
                        <img style={{width:'150px'}} src={props.photo}/>
                        :
                        <i class={"has-text-primary "+props.icon}></i>
                    }
                    
                </div>
                <div class="column">
                    <p>
                        <h2 class="title">{props.title}</h2>
                        <small class="subtitle">{props.subtitle}</small>
                    </p>
                </div>
                <div class="column is-narrow">
                    {props.rightComponent}
                </div>
            </div>
            </div>
            <hr/>
            
            
        </>
    )
}
PageHero.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
    icon: PropTypes.string,
    breadcrumb: PropTypes.array,
    rightComponent: PropTypes.element
}
PageHero.defaultProps = {
    title: "Títlo da Tabela",
    subtitle: "SubTítulo da Tabela",
    icon: "fa-clipboard",
    breadcrumb: [],
    rightComponent: <div>RIGHT</div>
}
export default PageHero