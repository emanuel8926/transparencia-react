
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { CSVLink } from "react-csv";
import exportFromJSON from 'export-from-json'

const Export = (props) => {
    const types = {
        csv: (
            <CSVLink
                data={props.data}
                filename={props.filename+".csv"}
                className="button is-info"
                separator={";"}
                >
                <span class="icon">
                    <i class="fas fa-file-download"></i>
                </span>
                <span>CSV</span>
            </CSVLink>
        ),
        xls: (
            <a class="button is-info" onClick={()=>{exportFromJSON({ data: props.data, fileName:props.filename, exportType:'xls' })}}>
                <span class="icon">
                    <i class="fas fa-file-excel"></i>
                </span>
                <span>XLS</span>
            </a>
        ),
        json: (
            <a class="button is-info" onClick={()=>{exportFromJSON({ data: props.data, fileName:props.filename, exportType:'json' })}}>
                <span class="icon">
                    <i class="fas fa-file-code"></i>
                </span>
                <span>JSON</span>
            </a>
        )
    }

    return (
        <>
            <div class="buttons has-addons">
                {
                    props.types.map(tp=>(
                        <>
                            {types[tp]}
                        </>
                    ))
                }
            </div>
            
        </>
    )
}
Export.propTypes = {
    filename: PropTypes.string,
    data: PropTypes.array,
    types: PropTypes.array
}
Export.defaultProps = {
    filename: "download",
    data: [],
    types: ["csv","xls","json"]
}
export default Export