import React, { useState, useEffect} from 'react'
import SocialShare from './socialShare'
import Loading from './loading'
import PageHero from './pageHero'
import { getTableConfig } from '../lib/tableFunctions'
import MainTable from './mainTable'
import { getAll } from '../lib/transparencia'
import {Dialog} from 'primereact/dialog'
import moment from 'moment'


const Base = ({slug,haveModal=false}) => {  
  const [data, setData] = useState([])
  const [cfg, setCfg] = useState(getTableConfig(slug))
  const [modalShow, setModalShow] = useState(false)
  const [rowSelected, setRowSelected] = useState({})
  const [loaded, setLoaded] = useState(false)
  const [url, setUrl] = useState('')

  useEffect(() => {
    if(!loaded){
      getAll(slug).then(r=>{
        setData(r)
        setCfg(getTableConfig(slug,r))
        setLoaded(true)
        setUrl(window.location.href)
      })
    }
  },[])
  var dataSelected = []
  if(loaded && haveModal){
    dataSelected = cfg.modalHeader.map(h=>{
      if(rowSelected[h.field]){
        if(h.type==='date'){
          if(moment(rowSelected[h.field]).isValid()){
            return moment(rowSelected[h.field]).format('DD/MM/YYYY')
          }
        }else{
          return rowSelected[h.field]
        }
      }
    })
    
  }
  
  return (
    <>
    
    <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
    {
      !loaded?
      <Loading loading={loaded} text={" "+cfg.title}/>
      :
      (data || []).length === 0?
        <>
          <strong style={{fontSize:'30px'}}>Sem resultados para exibir.</strong>
          <h1>Nenhuma registro encontrado...</h1>
        </>
        :
        <>
          <MainTable config={cfg} data={data}  customProps={
              haveModal?{
                selectionMode:"single",
                selection:{rowSelected},
                onSelectionChange: e => {
                  setRowSelected(e.value)
                  setModalShow(true)
                }}:{}
            }/>
            
          {
            haveModal?
            <Dialog header={cfg.title+" - "+dataSelected.join(' - ')} visible={modalShow} style={{width: '50vw'}} modal={true} onHide={() => setModalShow(false)}>
            {
              cfg.modal.map(v=>{
                var val = rowSelected[v.field]
                var key = v.header
                return (
                  <p>
                    <b>{key}</b>: {val}
                  </p>
                )

              })
            }
          </Dialog>
          :
          ''
          }
        </>
    }
      
    </>
  );
};
export default Base;