import PropTypes from 'prop-types'

const StatusColumn = ({data}) => {
    if(data.status_msg !== 'respondida' && data.status_msg !== 'indeferida'){
        return (
            <buton class={"button is-warning"} style={{width:"120px"}} disabled>Aguardando</buton>
        )
    }
    const toggleStyles = (event) => {
        document.querySelector('#modal-sic-resposta-'+data.protocolo).classList.toggle('is-active')
    }
    return (
        <>
            {
                data.status_msg === 'indeferida'?
                <buton class={"button is-danger"} style={{width:"120px"}} onClick={toggleStyles}>Indeferida</buton>
                :
                <buton class={"button is-success"} style={{width:"120px"}} onClick={toggleStyles}>Respondida</buton>
            }
            
            <div class="modal" id={"modal-sic-resposta-"+data.protocolo}>
            <div class="modal-background"></div>
            <div class="modal-card">
                <header class="modal-card-head has-text-left">
                <p class="modal-card-title" style={{fontSize:16}}>Detalhes da solicitação: </p>
                <button class="delete" aria-label="close" onClick={toggleStyles}></button>
                </header>
                <section class="modal-card-body has-text-left">
                
                    <b>Protocolo:</b> {data.protocolo}<br/>
                    <b>Resposta:</b><br/>
                    {data.resposta}<br/>

                
                </section>
                <footer class="modal-card-foot">
                <button class="button is-danger is-outlined" onClick={toggleStyles}>Fechar</button>
                </footer>
            </div>
            </div>
            
            </>
    )
}
StatusColumn.propTypes = {
    data: PropTypes.object
}
export default StatusColumn