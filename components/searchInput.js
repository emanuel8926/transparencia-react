
import {Button} from "primereact/button";
import {InputText} from "primereact/inputtext";
import React, { useState } from 'react'
import PropTypes from 'prop-types'

const SearchInput = (props) => {
    const [searchText, setSearchText] = useState('')

    return (
        <>
            <div className="p-col-12 p-md-4">
                <div className="p-inputgroup">
                    {
                        props.autosearch?(
                            <span className="p-inputgroup-addon">
                                <i className="pi pi-search"></i>
                            </span>
                        ):''
                    }
                    <InputText placeholder={props.placeholder} onKeyDown={(e)=>{
                            return e.key === 'Enter'?props.onSearch(searchText):''
                        }
                    } onKeyUp={(e)=>{
                            if(props.autosearch){
                                return props.onSearch(searchText)
                            }
                        }
                    }
                    onChange={(e) => setSearchText(e.target.value)}/>
                    {
                        props.autosearch?'':<Button icon="pi pi-search" className="p-button-primary" onClick={()=> props.onSearch(searchText)}/>
                    }
                </div>
            </div>
            
        </>
    )
}
SearchInput.propTypes = {
    searchFunction: PropTypes.func,
    placeholder: PropTypes.string,
    autosearch: PropTypes.bool
}
SearchInput.defaultProps = {
    placeholder: "Buscar",
    onSearch: () => {},
    autosearch: false
}
export default SearchInput