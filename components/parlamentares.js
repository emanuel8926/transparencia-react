import striptags from 'striptags'
import {AllHtmlEntities} from 'html-entities'
import Link from 'next/link'
import config from '../config.json'
import { getParlamentares } from '../lib/default'
import React, { useState, useEffect } from 'react'
import Loading from './loading.js';

function Parlamentares ({pCount=null,onLoaded=()=>{}}) {
  const [parlamentares, setParlamentares] = useState([])
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    if(!loaded){
      getParlamentares().then(r=>{
        //console.log(r)
        setParlamentares(r)
        setLoaded(true)
        onLoaded(r)
      })
    }
  },[])
  const setCargo = (cargo) => {
    switch(cargo){
        case 'presidente': return "Presidente";
        case 'vice_presidente': return "Vice-Presidente";
        case '1_secretario': return "1º Secretário";
        case '2_secretario': return "2º Secretário";
        case 'vereador': return "Vereador";
    }
  }
  if(!loaded){
    return (
      <Loading loading={true} />
    )
  }
  if(loaded && parlamentares.length === 0){
    return (
      <>
        <strong style={{fontSize:'30px'}}>Nenhum parlamentar encontrado...</strong>
        
      </>
    )
  }
  return (
    <section class="container">
      <div class="columns is-multiline">
      {
        (pCount?parlamentares.slice(0,pCount):parlamentares).map(parlamentar=>{
          const {
            nome,
            photo,
            naturalidade,
            profissao,
            legislacao,
            email,
            graduacao,
            data_nascimento,
            partido,
            mandato,
            slug,
            cargo 
          } = parlamentar
          return (
            <div class="column is-3">
              <div class="card " >
                <div class="card-image">
                  <figure class="image " className="">
                    <img src={photo} className="imageParlamentarGeneric" />
                  </figure>
                </div>
                <div class="card-content" >
                  <article class="media">
                    <div class="media-content">
                      <div class="content">
                        <p>
                          <div className="titlePostGeneric">
                            <strong className="titlePostGeneric">{AllHtmlEntities.decode(striptags(nome))}</strong> 
                          </div>
                          <div className="subTitlePostGeneric">
                            <p className="subTitlePostGeneric has-text-grey"><small>{setCargo(cargo)}</small> - <small>{partido}</small></p>
                          </div>
                          
                          <Link href={'/parlamentares/'+slug}>
                            <a>Acessar página</a>
                          </Link>
                        </p>
                      </div>
                      
                    </div>
                  </article>
                </div>
              </div>
            </div>
          )
        })
      }
      </div>
    </section>
  )
}


export default Parlamentares