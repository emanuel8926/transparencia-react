import striptags from 'striptags'
import {AllHtmlEntities} from 'html-entities'
import Link from 'next/link'
import config from '../config.json'
import { getPosts } from '../lib/postsFetch'
import React, { useState, useEffect } from 'react'

function Posts ({postsCount=null,onLoaded=()=>{}}) {
  const [posts, setPosts] = useState([])
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    if(!loaded){
      getPosts(config.api.provider).then(r=>{
        //console.log(r)
        setPosts(r)
        setLoaded(true)
        onLoaded(r)
      })
    }
  },[])
  if(!loaded){
    return (
      <>
        <h1>Carregando Notícias</h1>
        <progress class="progress is-small is-primary" max="100">15%</progress>
      </>
    )
  }
  if(loaded && posts.length === 0){
    return (
      <>
        <strong style={{fontSize:'30px'}}>Nenhuma notícia...</strong>
        <h1>Nenhuma notícia foi postada ainda...</h1>
      </>
    )
  }
  return (
    <section class="container">
      <div class="columns is-multiline">
      {
        (postsCount?posts.slice(0,postsCount):posts).map(post=>{
          const {
            ID,
            TITLE,
            DATE,
            SLUG,
            DESCRIPTION,
            AUTHOR,
            IMAGE,
            TAGS
          } = post
          return (
            <div class="column is-4">
              <div class="card " >
                <div class="card-image">
                  <figure class="image " className="imagePostGeneric">
                    <img src={IMAGE} className="imagePostGeneric" />
                  </figure>
                </div>
                <div class="card-content" >
                  <article class="media">
                    <div class="media-content">
                      <div class="content">
                        <p>
                          <div className="titlePostGeneric">
                            <strong className="titlePostGeneric">{AllHtmlEntities.decode(striptags(TITLE))}</strong> 
                          </div>
                          <div className="subTitlePostGeneric">
                            <p className="subTitlePostGeneric has-text-grey"><small>{DATE}</small> - <small>{AUTHOR.first_name}</small></p>
                          </div>
                          
                          <div className="bodyPostGeneric">
                            {DESCRIPTION}
                          </div>
                          <Link href={'/noticias/'+SLUG}>
                            <a>Leia mais</a>
                          </Link>
                        </p>
                      </div>
                      
                    </div>
                  </article>
                </div>
              </div>
            </div>
          )
        })
      }
      </div>
    </section>
  )
}


export default Posts