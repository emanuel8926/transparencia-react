import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import TableHeader from './tableHeader'

class Table extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sortOrder: 1,
            expandedRows: {}
        };
    }
    render() {        
        const { rows,columns,rowsPerPage,customProps} = this.props

        return (
            <div>
                <div className="content-section implementation">
                    <DataTable 
                        ref={(el) => this.dt = el} 
                        value={rows} 
                        paginator={true} 
                        rows={rowsPerPage} 
                        header={this.props.header?<TableHeader {...this.props.header}/>:<></>}
                        globalFilter={this.props.filter} 
                        emptyMessage="Nenhum registro encontrado..."  
                        {...customProps}
                    >
                        {
                            columns.map((c,i)=>{
                                return (<Column key={i} {...c} />)
                            })
                        }
                    </DataTable>
                </div>
            </div>
        );
    }
}
Table.propTypes = {
    rows: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    rowsPerPage: PropTypes.number,
    header: PropTypes.object,
    customProps: PropTypes.object,
}
Table.defaultProps = {
    expandable: false,
    title: '',
    subtitle: '',
    rowsPerPage: 10,
    customProps: {}
}

export default Table