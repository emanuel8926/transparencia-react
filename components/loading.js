import PropTypes from 'prop-types'
import striptags from 'striptags'
import {AllHtmlEntities} from 'html-entities'
import Link from 'next/link'

const Loading = ({loading=false,text=''}) => {

    return (
        <>
            <h1>Carregando{text}...</h1>
            <progress class="progress is-small is-primary" max="100">15%</progress>
        </>
    )
}
export default Loading