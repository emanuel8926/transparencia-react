
import config from '../config.json'
import React, { useState, useEffect } from 'react'
import { getAll } from '../lib/transparencia'
import Loading from './loading'
import DateColumn from './dateColumn'
import DownloadButton from './downloadButton'
import MainTable from './mainTable'
import Table from './table'
import { getTableConfig } from '../lib/tableFunctions'

function Publicacoes ({pubCount=null,onLoaded=()=>{},table=false }) {
  const [publicacoes, setPubs] = useState([])
  const [tabActive, setActiveTab] = useState(null)
  const [cfg, setCfg] = useState(getTableConfig('publicacoes'))
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    if(!loaded){
      getAll("publicacoes").then(r=>{
        var types = {};
        if(r){
          (pubCount?r.slice(0,pubCount):r).forEach(p=>{
            if(types[p.tipo]){
              types[p.tipo].push(p)
            }else{
              types[p.tipo] = []
              types[p.tipo].push(p)
            }
          })
        }
        
        setCfg(getTableConfig('publicacoes',r))
        setActiveTab(Object.keys(types).length > 0?Object.keys(types)[0]:null)
        setPubs(types)
        setLoaded(true)
        onLoaded(r)
      })
    }
  },[])
  if(!loaded){
    return (
      <>
        <Loading loading={true} text={" Publicações"} />
      </>
    )
  }
  if(publicacoes.length === 0){
    return <>
      <div class="notification is-warning">
        Nenhuma publicação encontrada...
      </div>
    </>
  }
  return (
    <div  >
      <div class="tabs is-toggle  ">
        <ul>
          {
            Object.keys(publicacoes).map((tipo,i)=>{
              return (
                <li class={tabActive===tipo?"is-active":""} onClick={()=>setActiveTab(tipo)}>
                
                  <a class="">
                    <span>{tipo}</span>
                  </a>

                </li>
              )
            })
          }
        </ul>
      </div>
      <div>
        {
          publicacoes[tabActive].length > 0?
          table?
          <MainTable config={cfg} data={publicacoes[tabActive]} />
          :
          publicacoes[tabActive].map(v=>{
            return (
              <div class="card">
                <header class="card-header">
                  <p class="card-header-title">
                    <DateColumn value={v.data}/> 
                  </p>
                </header>
                <div class="card-content">
                  <div class="content">
                    <div class="columns">
                      <div class="column has-text-justified">
                      {v.descricao}
                      </div>
                      <div class="column is-narrow">
                      <DownloadButton href={v.arquivo} fileId={v.file || 0} text="Download" />
                      </div>
                    </div>
                     
                  </div>
                </div>
              </div>

            )
          })
          :
          ""
        }
      </div>
    </div>
  )
}
export default Publicacoes

/*
<Table
  title={cfg.title}
  rows={publicacoes[tabActive]}
  columns={cfg.columns}
  rowsPerPage={5}
/>
*/