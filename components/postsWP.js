import striptags from 'striptags'
import {AllHtmlEntities} from 'html-entities'

function Posts ({posts=[]}) {
  
  if(posts.length === 0){
    return (
      <>
        <h1>Carregando Notícias</h1>
        <progress class="progress is-small is-primary" max="100">15%</progress>
      </>
    )
  }
  return (
    <section class="container">
      <div class="columns is-multiline">
      {
        (posts).map(post=>{
          const {
            TITLE,
            DATE,
            SLUG,
            DESCRIPTION,
            CATEGORIES,
            AUTHOR,
            IMAGE,
            TAGS
          } = post
          return (
            <div class="column is-6">
            <div class="box ">
              <article class="media">
                <div class="media-content">
                  <div class="content">
                    <p>
                      <strong>{AllHtmlEntities.decode(striptags(TITLE))}</strong> 
                      <p class="has-text-grey"><small>{DATE}</small> - <small>{AUTHOR.name}</small></p>
                      {AllHtmlEntities.decode(striptags(DESCRIPTION)).replace('Leia mais', '')}
                    </p>
                  </div>
                  <nav class="level is-mobile">
                    <div class="level-left">
                      <div class="tags">
                        {
                          TAGS.map(t=>{
                            return (
                              <span class="tag is-link">{t.name}</span>
                            )
                          })
                        }
                      </div>
                    </div>
                    <div class="level-right is-hidden">
                      <a class="level-item" aria-label="reply">
                        <span class="icon is-small">
                          <i class="fas fa-reply" aria-hidden="true"></i>
                        </span>
                      </a>
                      <a class="level-item" aria-label="retweet">
                        <span class="icon is-small">
                          <i class="fas fa-retweet" aria-hidden="true"></i>
                        </span>
                      </a>
                      <a class="level-item" aria-label="like">
                        <span class="icon is-small">
                          <i class="fas fa-heart" aria-hidden="true"></i>
                        </span>
                      </a>
                    </div>
                  </nav>
                  
                </div>
              </article>
            </div>
            </div>
          )
        })
      }
      </div>
    </section>
  )
}


export default Posts