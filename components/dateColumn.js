
import React, { useState,useEffect } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'

const DateColumn = (props) => {
    const [value, setValue] = useState('')
    useEffect(()=>{
        if(props.format==='competencia'){
            if(moment(props.value, 'DD/MM/YYYY').isValid()){
                setValue(moment(props.value, 'DD/MM/YYYY').format('MM/YYYY'))
            }else{
                if(props.value.includes('13')){
                    setValue('13/'+props.value.replace('01/13/','') )
                }else{
                    setValue('')
                }
            }
        }else{
            if(moment(props.value).isValid()){
                setValue(moment(props.value).format('DD/MM/YYYY'))
            }else{
                setValue('')
            }
        }
    })
    return (
        <>
            {value}
        </>
    )
}
DateColumn.propTypes = {
    value: PropTypes.string,
    format: PropTypes.string
}
export default DateColumn