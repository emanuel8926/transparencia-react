
import React, { useState,useEffect } from 'react'
import PropTypes from 'prop-types'
import currencyFormatter from'currency-formatter'


const MoneyColumn = (props) => {
    const [value, setValue] = useState('')

    const toVal = (val) => {
        val = String(val)
        if(val.indexOf(',') !== -1 && val.indexOf('.') !== -1){
            return val.replace(/[.]/g,'').replace(',','.')
        }else if(val.indexOf(',') !== -1 && val.indexOf('.') === -1){
            return val.replace(',','.')
        }else if(val.indexOf(',') === -1 && val.indexOf('.') !== -1){
            return val
        }else{
            return val
        }
    }

    useEffect(()=>{
        var val = toVal(props.value)
        if(val){
            
            setValue(currencyFormatter.format(val, {
                symbol: 'R$',
                decimal: ',',
                thousand: '.',
                precision: 2,
                format: '%s %v' // %s is the symbol and %v is the value
              }))
        }else{
            setValue('Valor inválido.')
        }
    })
    return (
        <>
            {value}
        </>
    )
}
MoneyColumn.propTypes = {
    value: PropTypes.string
}
export default MoneyColumn