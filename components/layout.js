import Head from 'next/head'
import Link from 'next/link'
//import '../node_modules/bulma/bulma.sass'
//import '../node_modules/react-image-gallery/styles/scss/image-gallery.scss'
import '../styles/styles.sass'
import { NextSeo } from 'next-seo'
import config from '../config.json'
import Hero from './hero' 
import MenuLateral from './menuLateral'
import React, { useState, useEffect } from 'react'
import moment from 'moment'

export default ({ children, url }) => {
  const [ano,setAno] = useState(moment().format('YYYY'))
  useEffect(() => {
    window.dataLayer = window.dataLayer || []
    function gtag() {
      dataLayer.push(arguments)
    }
    gtag('js', new Date())
    gtag('config', config.gtag, {
      page_location: window.location.href,
      page_path: window.location.pathname,
      page_title: window.document.title,
    })
  },[])
  return (
    <div  >
      <NextSeo
        title={config.siteMeta.title}
        description={config.siteMeta.description}
      />
      <Head>
        <script async src={"https://www.googletagmanager.com/gtag/js?id="+config.gtag}></script>
        <link rel="shortcut icon" href="/static/img/bandeira.png" />
        <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
      </Head>
      <Hero url={url}/>
      <br/>
      <div class="container">
        <div class="columns" >
          
          <div class="column is-hidden-touch is-narrow" >
            <MenuLateral/>
          </div>
          <section class="section " >
          <div class="container">
          {children}
          </div>
          </section>
          
        </div>
      
      </div>
      <br/>
      <nav class="navbar is-link is-bold" >
        
      </nav>
      <footer className="footer is-primary">
        <section class="section">
        <div className="container  has-text-white has-text-left">
          <div class="columns is-centered" style={{width:"100%"}}>
            
            <div class="column is-3 " >
              <b style={{fontSize:15,color:'lightgrey'}}>Câmara Municipal</b><br/>
              <b style={{fontSize:25}}>{config.siteMeta.municipio}</b>
              <br/>
              
              <p><b>CNPJ:</b> {config.siteMeta.cnpj}</p>
              <br/>
              <p>
                <b>Horário de Funcionamento:</b>
              </p>
              
              {
                config.siteMeta.funcionamento.map(h=>{
                  return (
                    <p><b>{h.dia}: </b> {h.horario}</p>
                  )
                })
              }
              
            

            {
              config.siteMeta.funcionamento_obs?
              <p><b>OBS.: </b>{config.siteMeta.funcionamento_obs}</p>
              :''
            }
            </div>
            <div class="column  is-2 ">
            </div>

            <div class="column  is-3 ">
              <p><b>Endereço:</b></p>

              <p>{config.siteMeta.endereco.logradouro}, {config.siteMeta.endereco.numero},</p>
              <p>{config.siteMeta.endereco.cidade}, {config.siteMeta.endereco.estado}, </p>
              <p><b>CEP:</b> {config.siteMeta.endereco.CEP}</p>
              <p><b>Telefone:</b>  {config.siteMeta.endereco.telefone}</p>
              <p><b>Email:</b>  {config.siteMeta.email}</p>
              
            </div>
            <div class="column  is-1 ">
            </div>
            
            <div class="column is-3">
              <p><h1><b>Siga-nos:</b></h1></p>
              <br/>
              <i class="fab fa-3x fa-facebook-square has-text-white" style={{margin:5}} aria-hidden="true"></i>
              <i class="fab fa-3x fa-twitter-square has-text-white" style={{margin:5}} aria-hidden="true"></i>
              <i class="fab fa-3x fa-instagram has-text-white" style={{margin:5}} aria-hidden="true"></i>
              <i class="fab fa-3x fa-youtube has-text-white" style={{margin:5}} aria-hidden="true"></i>
            </div>
          </div>
          <hr/>
          <div class="has-text-centered">
            @ {ano} Todos os direitos reservados
          </div>
        </div>
        </section>
      </footer>
    </div>
  )
}