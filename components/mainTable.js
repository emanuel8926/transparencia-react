import React, { useState, useEffect} from 'react'
import PropTypes from 'prop-types'

import Table from './table'
import SearchInput from './searchInput'
import Export from './export'



const MainTable = ({config:cfg,data,customProps}) => {  
  const [tbfilter, setTbfilter] = useState('')
  
  if(data.length === 0){
    return (
      <>
        <h1>Carregando...</h1>
        <progress class="progress is-small is-primary" max="100">15%</progress>
      </>
    )
  }
  const headerRight = <Export filename={cfg.slug} data={data}/>
  const headerLeft = <SearchInput autosearch={true} onSearch={(txt)=>setTbfilter(txt)} />
 
  return (
    <>
      <Table
        title={cfg.title}
        header={{
          rightComponent: headerRight,
          leftComponent: headerLeft,
        }}
        rows={data}
        columns={cfg.columns}
        filter={tbfilter}
        rowsPerPage={7}
        customProps={customProps}
      />
    </>
  );
};


MainTable.propTypes = {
    config: PropTypes.object.isRequired,
    data: PropTypes.array.isRequired,
    customProps: PropTypes.object
}
export default MainTable;