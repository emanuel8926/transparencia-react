
import config from '../config.json'
import React, { useState, useEffect } from 'react'
import { getAll } from '../lib/transparencia'
import Loading from './loading'
import DateColumn from './dateColumn'
import DownloadButton from './downloadButton'
import MainTable from './mainTable'
import Table from './table'
import { getTableConfig } from '../lib/tableFunctions'

function Pautas ({pubCount=null,onLoaded=()=>{},table=false }) {
  const [pautas, setPautas] = useState([])
  const [cfg, setCfg] = useState(getTableConfig('pautas'))
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    if(!loaded){
      getAll("pautas").then(r=>{
        if(r){
          setPautas(r)
        }
        setCfg(getTableConfig('pautas',r))
        setLoaded(true)
        onLoaded(r)
      })
    }
  },[])
  if(!loaded){
    return (
      <>
        <Loading loading={true} text={" Pautas"} />
      </>
    )
  }
  if(pautas.length === 0){
    return <>
      <div class="notification is-warning">
        Nenhuma pauta encontrada...
      </div>
    </>
  }
  return (
    <div  class="notification is-white">
      <div>
        {
          pautas.length > 0?
          table?
          <MainTable config={cfg} data={pautas[tabActive]} />
          :
          pautas.map(v=>{
            return (
              <article class="media">
                <div class="media-content">
                  <div class="content">
                    <p>
                      <small><DateColumn value={v.data}/></small>
                      <br/>
                    </p>
                  </div>
                </div>
                <div class="media-right">
                  <DownloadButton text="Download" fileId={v.arquivo} />
                </div>
              </article>
            )
          })
          :
          ""
        }
      </div>
    </div>
  )
}
export default Pautas

/*
<Table
  title={cfg.title}
  rows={pautas[tabActive]}
  columns={cfg.columns}
  rowsPerPage={5}
/>
*/