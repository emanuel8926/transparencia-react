import Link from 'next/link'

const eSicAds = () => {

    return (
        <section class="notification is-warning">
          <div class="columns">
            <div class="column is-narrow">
              <p class="">
                <img src="/static/img/sic.png" style={{width:"200px"}} />
              </p>
            </div>
            <div class="column ">
              <p style={{color:'darkgreen'}}>
                <strong style={{color:'darkgreen',fontSize:26}}>Receba sua resposta em até 15 dias</strong><br/>
                <span>
                Tenha acesso à informações públicas sobre a Câmara.  
                </span>
              </p>
            </div>
            <div class="column is-narrow ">
              <Link href="/transparencia/e-sic">
                <a class="button is-dark is-outlined"> <b >Clique aqui para acessar</b></a>
              </Link>
            </div>
          </div>

        </section>
    )
}
export default eSicAds