import PropTypes from 'prop-types'
import striptags from 'striptags'
import {AllHtmlEntities} from 'html-entities'
import Link from 'next/link'

const Breadcrumb = ({links}) => {

    return (
        <nav class="breadcrumb" aria-label="breadcrumbs">
            <ul>
                {links.map(info=>{
                    if(info.active){
                        return (<li class="is-active"><a href="#" aria-current="page">{AllHtmlEntities.decode(striptags(info.title))}</a></li>)
                    }else{
                        return (
                            <li>
                                <Link href={info.link}>
                                <a>{info.title}</a>
                                </Link>
                            </li>
                        )
                    }
                })}
            </ul>
        </nav>
    )
}
Breadcrumb.propTypes = {
    links: PropTypes.array
}
Breadcrumb.defaultProps = {
    links: []
}
export default Breadcrumb