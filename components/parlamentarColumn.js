
import React, { useState,useEffect } from 'react'
import PropTypes from 'prop-types'
import {Dialog} from 'primereact/dialog'
import {Button} from 'primereact/button';

const ParlamentarColumn = ({parlamentarData,rowData}) => {
    const [isLoading, setLoading] = useState(false)
    const [modalShow, setModalShow] = useState(false)

    if(isLoading){
        return (
          <>
            <h1>Carregando...</h1>
            <progress class="progress is-small is-primary" max="100">15%</progress>
          </>
        )
      }
    const toggleStyles = (event) => {
        document.querySelector('#modal').classList.toggle('is-active')
    }
    const setCargo = (cargo) => {
        switch(cargo){
            case 'presidente': return "Presidente";
            case 'vice_presidente': return "Vice-Presidente";
            case '1_secretario': return "1º Secretário";
            case '2_secretario': return "2º Secretário";
            case 'vereador': return "Vereador";
        }
    }
    return (
        <>
            {Object.keys(parlamentarData).length > 0?
                <>
                    <Button label="Primary" label={parlamentarData.nome}  onClick={toggleStyles} />
                    <div id="modal" class="modal">
                        <div class="modal-background"></div>
                        <div class="modal-card">
                            <article class="message is-primary">
                                <div class="message-header">
                                    <p>{parlamentarData.nome}</p>
                                    <button class="delete" aria-label="delete" onClick={toggleStyles}>></button>
                                </div>
                                <div class="card">
                                    <div class="card-image">
                                        <figure class="image is-4by3">
                                            <img src={parlamentarData.photo}/>
                                        </figure>
                                    </div>
                                    <div class="card-content">
                                        <div class="media">
                                        <div class="media-content">
                                            <p class="title is-4">{parlamentarData.nome} ({parlamentarData.apelido})</p>
                                            <p class="subtitle is-6">{setCargo(parlamentarData.cargo)}</p>
                                        </div>
                                        </div>

                                        <div class="content has-text-left">
                                            {JSON.stringify(parlamentarData,null,2)}
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </>
            :"--"
            }
            
        </>
    )
}
ParlamentarColumn.propTypes = {
    parlamentarData: PropTypes.object,
    rowData: PropTypes.object
}
ParlamentarColumn.defaultProps = {
    parlamentarData: {},
    rowData: {}
}
export default ParlamentarColumn