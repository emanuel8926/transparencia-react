
import SearchInput from './searchInput'
import Link from '../components/link'
import config from '../config.json'
import { getLinkDespesas, getLinkReceitas } from '../lib/default'
import React, { useState, useEffect} from 'react'
import Router from 'next/router'

const SideMenu = () => {
  const [pageSlug,setPageSlug] = useState('')
  const [loaded, setLoaded] = useState(false)
  const [receitas,setReceitas] = useState('')
  const [despesas,setDespesas] = useState('')

  const setMenuActive = (url) => {
    const slug = url.split('/').slice(-1)[0]
    setPageSlug(slug)
  }

  useEffect(()=>{
    
    setMenuActive(window.location.href)
  })

  const sidemenu = config.siteMeta.sideMenu

  const search = (txt) => {
    window.location.href='/busca?value='+txt
  }

  return (
    <nav class="panel" style={{width:220}}>
      <div class="field">
        <SearchInput placeholder="O que você procura?" onSearch={(txt)=>{search(txt)}}/>
      </div>
    {
      sidemenu.map(s=>{
        switch(s.type){
          default:
            return (
              <nav class="panel">
                <p class="panel-heading">
                  {s.title}
                </p>
                {s.children.map(c=>{
                  return <Link title={c.title} mainClass="panel-block" slug={c.slug} route={s.route} icon={c.icon} />
                }
                )}
              </nav>
            )
        }
      })
    }
    </nav>
    
  )
}
export default SideMenu