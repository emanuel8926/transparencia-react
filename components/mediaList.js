import React from 'react';
import striptags from 'striptags'
import {AllHtmlEntities} from 'html-entities'
import { MDBMedia } from 'mdbreact';
import Link from 'next/link'

const MediaObjectPage = ({posts=[]}) => {
    if(posts.length === 0){
        return (
          <>
            <h1>Carregando Notícias</h1>
            <progress class="progress is-small is-primary" max="100">15%</progress>
          </>
        )
      }
  return (
    <>
    {
        (posts).map(post=>{
          const {
            ID,
            TITLE,
            DATE,
            SLUG,
            DESCRIPTION,
            AUTHOR,
            TAGS,
            IMAGE
          } = post
          return (
            <div class="box ">
                <MDBMedia list className="mt-3">
                  <MDBMedia tag="li">
                    <MDBMedia left href="#" >
                        <MDBMedia object src={IMAGE} style={{width:"150px"}} alt="Generic placeholder image" />
                    </MDBMedia>
                    <MDBMedia body>
                      <MDBMedia heading>
                            <strong>{AllHtmlEntities.decode(striptags(TITLE))}</strong> 
                            <p class="has-text-grey"><small>{DATE}</small> - <small>{AUTHOR.first_name}</small></p>
                      </MDBMedia>
                        {DESCRIPTION}
                        <Link href={'/noticias/'+SLUG}>
                            <a>Leia mais</a>
                        </Link>
                        <div class="tags">
                                    {
                                    TAGS.map(t=>{
                                        return (
                                        <span class="tag is-link">{t}</span>
                                        )
                                    })
                                    }
                                </div>
                    </MDBMedia>
                  </MDBMedia>
                </MDBMedia>
            </div>
          )
        })
    }
    </>
  );
}

export default MediaObjectPage;