
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import Link from 'next/link'

const LinkItem = ({route,slug,icon,title,mainClass,iconClass,activeClass, href=null, target=""}) => {
    const [pageSlug,setPageSlug] = useState('')

    const setMenuActive = (url) => {
        const slug = url.split('/').slice(-1)[0]
        setPageSlug(slug)
    }
    useEffect(()=>{
        setMenuActive(window.location.href)
    })
    
    if(!href){
        href = slug===""?`/${route}`:`/${route}/${slug}`
    }
    const isActive = slug===""?pageSlug===route? true : false :pageSlug===slug? true : false

    return (
        <>
            <Link href={href}>
                <a target={target} class={`${mainClass} ${isActive?"is-active ":""}`}>
                    <span class={`panel-icon "+${isActive? activeClass+" has-text-weight-bold":""}`}>
                        <i class={icon+" "+iconClass} aria-hidden="true"></i> &nbsp;
                    </span>
                    <span class={`${isActive? activeClass+" has-text-weight-bold":""}`}>
                        {title}
                    </span>
                </a>
            </Link>
            
        </>
    )
}
LinkItem.propTypes = {
    route: PropTypes.string,
    slug: PropTypes.string,
    title: PropTypes.string,
    icon: PropTypes.string,
    href: PropTypes.string,
}
LinkItem.defaultProps = {
    icon: "fas fa-home",
    title: ""
}
export default LinkItem