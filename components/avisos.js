

export default ({config}) => {

    return (
        <section class="notification is-primary" >
          <div class="columns">
            <div class="column  is-narrow">
                <i class="fas fa-5x fa-phone-square"></i>
            </div>
            <div class="column ">
              <strong style={{fontSize:40}}>Fale Conosco</strong>
              <br/>
              
              <span style={{color:'cyan'}}>
                Expediente:  &nbsp; 
                {
                  config.siteMeta.funcionamento.map(h=>{
                    return (
                      <><b>{h.dia} - </b> {h.horario}<br/></>
                    )
                  })
                }
              </span>
              
            </div>
            <div class="column is-narrow">
              <span style={{fontSize:22,wordWrap:'break-word'}}>
                    <i class="fas fa-envelope"></i> 
                      &nbsp;
                    {config.siteMeta.email}
                </span><br/>
              <span style={{fontSize:22,wordWrap:'break-word'}}>
                    <i class="fas fa-phone"></i> 
                      &nbsp;
                    {config.siteMeta.endereco.telefone}
                </span><br/>
            </div>
          </div>

        </section>
    )
}
