
import React, { useState,useEffect } from 'react'
import PropTypes from 'prop-types'
import { getMediaFromDirectus } from '../lib/postsFetch'
import { getFile } from '../lib/default'
import axios from 'axios'
import Loading from './loading';

const DownloadButton = (props) => {
    const [filename, setFilename] = useState('')
    const [href, setHref] = useState('')
    const [isLoading, setLoading] = useState(false)

    useEffect(()=>{
        if(parseInt(props.fileId) === 0){
            if(props.href){
                setHref(props.href.replace('open?','uc?export=download&'))
                
            }
            setLoading(false)
        }else{
            if(!isLoading){
                getFile(props.fileId).then(r=>{
                    setFilename(r.title+'.'+r.data.full_url.slice(-3))
                    setHref(r.data.full_url)
                    setLoading(false)
                    
                })

            }
        }
        
    })
    if(isLoading){
        return (
          <Loading loading={true} />
        )
      }
    return (
        <>
            {props.fileId > 0?
                <a class="button is-info" onClick={()=>{
                    axios({
                        url: href,
                        method: 'GET',
                        responseType: 'blob', // important
                        
                      }).then((response) => {
                        const url = window.URL.createObjectURL(new Blob([response.data]));
                        const link = document.createElement('a');
                        link.href = url;
                        link.setAttribute('download', filename);
                        document.body.appendChild(link);
                        link.click();
                      });
                }}>
                    <span class="icon">
                        <i class={props.icon}></i>
                    </span>
                    <span>{props.text}</span>
                </a>
            :
            props.href?
                <a class="button is-info" href={href}>
                    <span class="icon">
                        <i class={props.icon}></i>
                    </span>
                    <span>{props.text}</span>
                </a>
                :"--"
            }
            
        </>
    )
}
DownloadButton.propTypes = {
    href: PropTypes.string,
    text: PropTypes.string,
    icon: PropTypes.string,
    fileId: PropTypes.number
}
DownloadButton.defaultProps = {
    text: "Download",
    icon: "fas fa-file-pdf",
    fileId: 0
}
export default DownloadButton