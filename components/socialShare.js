import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {FacebookShareButton,TwitterShareButton,TelegramShareButton,WhatsappShareButton,EmailShareButton } from 'react-share';
import { FacebookIcon,TwitterIcon,TelegramIcon,WhatsappIcon,EmailIcon } from 'react-share';

const SearchInput = (props) => {

    const networkComponents = {
        facebook: (
            <FacebookShareButton url={props.url}>
                <FacebookIcon size={props.iconsSize}  />
            </FacebookShareButton>
        ),
        twitter: (
            <TwitterShareButton url={props.url}>
                <TwitterIcon size={props.iconsSize} />
            </TwitterShareButton>
        ),
        whatsapp: (
            <WhatsappShareButton url={props.url}>
                <WhatsappIcon size={props.iconsSize} />
            </WhatsappShareButton>
        ),
        telegram: (
            <TelegramShareButton url={props.url}>
                <TelegramIcon size={props.iconsSize} />
            </TelegramShareButton>
        ),
        email: (
            <EmailShareButton url={props.url}>
                <EmailIcon size={props.iconsSize} />
            </EmailShareButton>
        )
    }

    return (
        <>
            <table>
                <tr>
                {
                    props.networks.map((net,i)=>(
                        <td key={i} >{networkComponents[net]} </td>
                    ))
                }
                </tr>
            </table>
            
        </>
    )
}
SearchInput.propTypes = {
    iconsSize: PropTypes.number,
    networks: PropTypes.array,
    url: PropTypes.string
}
SearchInput.defaultProps = {
    iconsSize: 30,
    url:"#",
    networks: ['facebook','twitter','whatsapp','telegram','email']
}
export default SearchInput