
import { NextSeo } from 'next-seo'
import config from '../config.json'
import Link from '../components/link'
import React, { useState, useEffect} from 'react'

export default ({ url }) => {
  const [pageSlug,setPageSlug] = useState('')

  const setMenuActive = (url) => {
    const slug = url.split('/').slice(-1)[0]
    setPageSlug(slug)
  }

  const toggleStyles = (event) => {
    document.querySelector('#burger').classList.toggle('is-active')
    document.querySelector('#navbarmenu').classList.toggle('is-active')
  }

  useEffect(()=>{
    setMenuActive(window.location.href)
  })

  const mainmenu = config.siteMeta.mainMenu


  return (
    <div>
      

      <section class="hero is-bold is-primary " >
        <div class="hero-body is-bold"  >
          <div class="container">
            
            <div class="columns">
              <div class="column is-narrow has-text-centered">
                
                <img style={{width:'130px'}} src="/static/img/bandeira.png" />
              </div>
              <div class="column">
                <h1 class="title" >
                  {config.siteMeta.title}
                </h1>
                <h4 class="subtitle " style={{color:'lightgrey'}}>
                  {config.siteMeta.subtitle}
                </h4>
              </div>
              <div class="column is-narrow">
              </div>
            </div>
          </div>
        </div>

      </section>
      <nav class="navbar is-link is-bold is-hidden-tablet" style={{width:"100%",position:"absolute"}} >
        <div class="container " >
          <div class="navbar-brand">
            <a id="burger" onClick={toggleStyles} 
            role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarmenu" style={{color:'white'}}>
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <div id="navbarmenu" class="navbar-menu ">
            <div class="navbar-start">
              {
                mainmenu.map(m=>{
                  if(m.link){
                    return (
                      <Link title={m.title} target="_blank" mainClass="navbar-item" iconClass="has-text-white" activeClass="has-text-white" href={m.link}  slug={m.slug} route={m.route} icon={m.icon} />
                    )
                  }else{
                    return (
                      <Link title={m.title} mainClass="navbar-item" iconClass="has-text-white" activeClass="has-text-white" slug={m.slug} route={m.route} icon={m.icon} />
                    )
                  }
                })
              }
            </div>
            <div class="navbar-end">
            </div>
          </div>
        </div>
      </nav>
      
      <div style={{height:"40px"}} class="is-hidden-tablet">

      </div>
      <nav class="navbar is-link is-bold is-hidden-mobile" >
        <div class="container ">
          <div class="navbar-brand">
            <a id="burger" onClick={toggleStyles} 
            role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarmenu" style={{color:'white'}}>
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <div id="navbarmenu" class="navbar-menu ">
            <div class="navbar-start">
              {
                mainmenu.map(m=>{
                  if(m.link){
                    return (
                      <Link title={m.title} target="_blank" mainClass="navbar-item" iconClass="has-text-white" activeClass="has-text-white" href={m.link}  slug={m.slug} route={m.route} icon={m.icon} />
                    )
                  }else{
                    return (
                      <Link title={m.title} mainClass="navbar-item" iconClass="has-text-white" activeClass="has-text-white" slug={m.slug} route={m.route} icon={m.icon} />
                    )
                  }
                })
              }
            </div>
            <div class="navbar-end">
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
}