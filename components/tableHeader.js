
import React, { useState } from 'react'
import PropTypes from 'prop-types'

const TableHeader = (props) => {
    const [searchText, setSearchText] = useState('')

    return (
        <>
            <nav class={"navbar "+props.navClass} role="navigation">
                <div id="navbarExampleTransparentExample" class="navbar-menu">
                    <div class="navbar-start">
                        <div class="navbar-item">
                        {props.leftComponent}
                        </div>
                    </div>

                    <div class="navbar-end">
                        <div class="navbar-item">
                        {props.rightComponent}
                        </div>
                    </div>
                </div>
            </nav>
            
            
        </>
    )
}
TableHeader.propTypes = {
    leftComponent: PropTypes.element,
    rightComponent:PropTypes.element,
    navClass: PropTypes.string
}
TableHeader.defaultProps = {
    navClass: "is-transparent",
    leftComponent: <div>LEFT</div>,
    rightComponent: <div>RIGHT</div>,
}
export default TableHeader