




module.exports = (server,app) => {
    const controllers = require('./controllers')(app)
    server.get('/noticias/:slug', controllers.noticias)
    server.get('/noticia/:slug', controllers.noticia);
    server.get('/parlamentares/:slug', controllers.parlamentares)
    server.get('/parlamentar/:slug', controllers.parlamentar);
    server.post('/api/sic/nova-solicitacao', controllers.novaSolicitaoSic);
}