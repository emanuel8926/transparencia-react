const mysql = require('mysql');
const { promisify } = require('util')
const md5 = require('md5')
const path = require('path')
const os = require('os')
const config = require('./config.json')
const moment = require('moment')

const prepareBodySolicitante = (data) => {
    var msg = '<p><b>Sua solicitação foi recebida!</b></p>'+
    '<p>Segue abaixo os dados da sua solicitação:</p>'+
    '<p><b>Protocolo:</b> '+data.protocolo+' </p>'+
    '<p><b>Solicitante: </b>'+data.nome_solicitante+'</p>'+
    '<p><b>Email do Solicitante: </b>'+data.email_solicitante+'</p>'+
    '<p><b>Área de Interesse: </b>'+data.area_solicitacao+'</p>'+
    '<p><b>Assunto: </b>'+data.assunto+'</p>'+
    '<p><b>Mensagem: </b>'+data.mensagem+'</p>'+
    '<br/>'+
    '<br/>'+
    '<p>Agradecemos o interesse.</p>'
    '<p>Em até 15 dias, nossa equipe entrará em contato!</p>'
    return msg
}
const prepareBodySic = (data) => {
    var msg = '<p><b>Nova solicitação de Informação!</b></p>'+
    '<p><b>Protocolo:</b> '+data.protocolo+' </p>'+
    '<p><b>Solicitante: </b>'+data.nome_solicitante+'</p>'+
    '<p><b>Email do Solicitante: </b>'+data.email_solicitante+'</p>'+
    '<p><b>Área de Interesse: </b>'+data.area_solicitacao+'</p>'+
    '<p><b>Assunto: </b>'+data.assunto+'</p>'+
    '<p><b>Mensagem: </b>'+data.mensagem+'</p>'+
    '<br/>'
    return msg
}
const prepareBodyResposta = (data) => {
    var msg = '<p><b>Sua solicitação foi respondida!</b></p>'+
    '<br/>'+
    '<p><b>Dados da solicitação:</b></p>'+
    '<p><b>Protocolo:</b> '+data.protocolo+' </p>'+
    '<p><b>Solicitante: </b>'+data.nome_solicitante+'</p>'+
    '<p><b>Email do Solicitante: </b>'+data.email_solicitante+'</p>'+
    '<p><b>Área de Interesse: </b>'+data.area_solicitacao+'</p>'+
    '<p><b>Assunto: </b>'+data.assunto+'</p>'+
    '<p><b>Mensagem: </b>'+data.mensagem+'</p>'+
    '<br/>'
    '<br/>'
    '<p><b>Resposta:</b></p>'+
    '<p>'+data.resposta+'</p>'
    return msg
}


const setEmailInQueue = async (dbQuery, data,type,emailsToSend=[], emailConfig) => {

    console.log(data)
    const { email_sender, email_sender_password, nome_orgao, email_enviar_msg_sic, smtp_host, smtp_port } = emailConfig[0]
    const configOk = [email_sender, email_sender_password, smtp_port, smtp_host, nome_orgao].every(v=>v)
    if(!configOk){
        
        await dbQuery('ROLLBACK;')
        throw new Error("Configuração de email inválida. "+JSON.stringify(emailConfig[0]))
    }
    var msg = ''
    var from = `${nome_orgao} <${email_sender}>`
    var subject = ''
    switch(type){
        case 'solicitante':
            subject = '[e-Sic] Dados da sua solicitação'
            emailsToSend.push(data.email_solicitante)
            msg = prepareBodySolicitante(data); break;
        case 'sic':
            subject = '[e-Sic] Nova solicitação de Informação!'
            emailsToSend = emailsToSend.concat( String(email_enviar_msg_sic).split(';'))
            msg = prepareBodySic(data); break;
        case 'resposta':
            subject = '[e-Sic] Sua solicitação foi respondida!'
            emailsToSend.push(data.email_solicitante)
            msg = prepareBodyResposta(data); break;
    }
    return await dbQuery('INSERT INTO email_queue (`from`,`to`,`subject`,`html`) VALUES (?,?,?,?)', [from, emailsToSend.join(";"),subject, msg ] )
   
}

const getEmailConfig = async ( dbQuery ) => {
    try {   
        return await dbQuery( 'SELECT * FROM email_config' )
    } catch (error) {
        console.error(error)
        return []
    }
}
const setSolicitacao = async (data) => {

    const protocolo = md5(JSON.stringify(data)+moment().format('DDMMYYHHmmss'))
    const databaseConfig = {
        connectionLimit : 10,
        host     : config.host,
        user     : config.user,
        password : config.pass,
        database : config.database
    }
    try{
        const pool = mysql.createPool(databaseConfig)
        const dbQuery = promisify(pool.query).bind(pool)
        const end = promisify(pool.end).bind(pool)

        const emailConfig = await getEmailConfig( dbQuery )
        if(!emailConfig.length){
            end()
            throw new Error("Configuração de email não encontrada.")
        }
        await dbQuery('START TRANSACTION;')

        const insertQuery = `INSERT INTO sic_mensagens_recebidas 
        (protocolo,nome_solicitante,email_solicitante,area_solicitacao,assunto,mensagem) VALUES
        (?,?,?,?,?,?)`
        await dbQuery( insertQuery, [protocolo, data.nome_solicitante, data.email_solicitante, data.area_solicitacao, data.assunto, data.mensagem] )
        const get = await dbQuery( 'SELECT protocolo FROM sic_mensagens_recebidas WHERE protocolo = ?', [protocolo] )
        if(!get.length){
            end()
            await dbQuery('ROLLBACK;')
            throw new Error("Erro ao gravar mensagem.")
        }
        try{
            data.protocolo = protocolo
            await setEmailInQueue( dbQuery, data,"solicitante",[],emailConfig)
            await setEmailInQueue( dbQuery, data,"sic",[],emailConfig)
            await dbQuery('COMMIT;')
            return get;
        }catch(err){
            await dbQuery('ROLLBACK;')
            throw new Error("Erro ao processar solicitação.")
        }

    }catch(err){
        console.log(err)
        throw new Error("Erro ao processar solicitação.")
    }
}

module.exports = {
    setSolicitacao
}
