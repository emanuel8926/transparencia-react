const { setSolicitacao } = require('./lib/db')
const fs = require('fs')
const moment = require('moment-timezone')

const timez = 'America/Maceio'
moment.tz.setDefault(timez)
const now = moment()

module.exports = (app) => {
    const noticias = (req,res)=>{
        const actualPage = '/noticia'
        const queryParams = { slug: req.params.slug } 
        app.render(req, res, actualPage, queryParams)
    }
    const noticia = (req, res) => {
        return app.render(req, res, '/noticia', { slug: req.params.slug });
    }
    
    const parlamentares = (req,res)=>{
        const actualPage = '/parlamentar'
        const queryParams = { slug: req.params.slug } 
        app.render(req, res, actualPage, queryParams)
    }
    const parlamentar = (req, res) => {
        return app.render(req, res, '/parlamentar', { slug: req.params.slug });
    }
    const novaSolicitaoSic = (req,res) => {
        const {nome_solicitante, email_solicitante, area_solicitacao, assunto, mensagem} = req.body
        if(!nome_solicitante || !email_solicitante || !area_solicitacao || !assunto || !mensagem){
            return res.status(401).json({success:false,msg:"Favor preencher todos os campos."})
        }else{
            setSolicitacao(req.body).then(r=>{
                return res.status(200).json({success:true,data: r})
            }).catch(err=>{
                console.log(err)
                fs.appendFileSync('./log.txt',"["+now.format('DD/MM/YYYY HH:mm:ss')+"][SITE][E-SIC] Erro ao salvar solicitação!: "+err+'\r\n')
                return res.status(200).json({success:false,msg:"Erro ao salvar solicitação!"})
            })
        }
    }
    return {
        noticia,
        noticias,
        parlamentar,
        parlamentares,
        novaSolicitaoSic
    }
}