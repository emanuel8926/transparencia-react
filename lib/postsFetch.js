
 
 
import { preparePostsProps,preparePostProps } from '../lib/postFunctions'
import fetch from 'isomorphic-unfetch'
import config from '../config.json'

const fetchCollections =  (cols) => {
  var allData = {}
  const baseUrl = config.api.urls.base

  const data = cols.map(async col=>{
    var url = baseUrl+config.api.urls[col]
    var data = await fetch(url)
    var json = await data.json()
    allData[col] = json.data
  })

  return Promise.all(data).then(r=>{
    return allData;
  })
}

 const wordpress = {
   getPosts: async () => {
    const baseUrl = config.api.urls.base

    const postsUrl = baseUrl+config.api.urls.posts
    const tagsUrl = baseUrl+config.api.urls.tags
    const categoriesUrl = baseUrl+config.api.urls.categories
    const mediaUrl = baseUrl+config.api.urls.media
    const usersUrl = baseUrl+config.api.urls.users

    const posts = await fetch(postsUrl)
    const jsonPosts = await posts.json()
    
    var tagsPosts = []
    var categPosts = []
    var mediaPosts = []

    jsonPosts.forEach(p=>{
      tagsPosts = [...tagsPosts, ...p.tags]
      categPosts = [...categPosts, ...p.categories]
      mediaPosts = [...mediaPosts, p.featured_media]
    })

    const tags = await fetch(tagsUrl+'?include='+tagsPosts.join(','))
    const categories = await fetch(categoriesUrl+'?include='+categPosts.join(','))
    const media = await fetch(mediaUrl+'?include='+mediaPosts.join(','))

    const users = await fetch(usersUrl)

    const jsonTags = await tags.json()
    const jsonCategories = await categories.json()
    const jsonMedia = await media.json()
    const jsonUsers = await users.json()
    //
    
    const postsData = preparePostsProps({ 
      posts: jsonPosts,
      tags: jsonTags,
      categories: jsonCategories,
      media: jsonMedia,
      users: jsonUsers 
    })
    return postsData
  }
 }
  
  const directus = {
    getPosts: async () => {
      const params = await fetchCollections(['posts', 'media', 'users'])
      
      return preparePostsProps(params)
    },
    getPost: async (slug) => {
      const baseUrl = config.api.urls.base
  
      const postsUrl = baseUrl+config.api.urls.posts+'&filter[slug]='+slug
      const posts = await fetch(postsUrl)
      
      const jsonPosts = await posts.json()
      const params = await fetchCollections(['media', 'users'])
      return preparePostsProps({
        posts: jsonPosts.data,
        media:params.media,
        users:params.users
      })

    } 
  }
  const providers = {
      wordpress,
      directus
  }

  export const getPosts = async (provider) => {
    return await providers[provider].getPosts()
  }
  export const getPost = async (provider,slug) => {
      return await providers[provider].getPost(slug)
  }
  export const getMediaFromDirectus = async () => {
    const coll = await fetchCollections(['media'])

    return coll['media']
  }