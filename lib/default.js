import fetch from 'isomorphic-unfetch'
import config from '../config.json'

const getComissoes = async () => {
  const baseUrl = config.api.urls.base

  var urlParlamentares = baseUrl+config.api.urls['parlamentares']
  
  const parlamentares = await fetch(urlParlamentares)
  const jsonParlamentares = await parlamentares.json()

  var parlam = {}
  jsonParlamentares.data.forEach(p=>{
    parlam[p.id] = p
  })


  const urlStruct = baseUrl+config.api.urls['estrutura-organizacional']

  const struct = await fetch(urlStruct)
  const jsonStruct = await struct.json()

  return jsonStruct.data.map(s=>{
    return {
      ...s,
      presidente : parlam[s.presidente],
      vice_presidente : parlam[s.vice_presidente],
      secretario : parlam[s.secretario],
      secretario2 : parlam[s.secretario2],
      relator : parlam[s.relator]
    }
  })

} 
const getParlamentares = async (slug=null) => {
  const baseUrl = config.api.urls.base

  let urlParlamentares = baseUrl+config.api.urls['parlamentares']
  if(slug){
    urlParlamentares += '&filter[slug]='+slug
  }
  const urlLegislacao = baseUrl+config.api.urls['legislacao']
  const urlPublicacoes = baseUrl+config.api.urls['publicacoes']
  const urlFiles = baseUrl+config.api.urls['media']

  const files = await fetch(urlFiles)
  const jsonFiles = await files.json()
  
  var filesById = {}
  jsonFiles['data'].forEach(f=>{
    filesById[f.id] = f
  })
  
  const leis = await fetch(urlLegislacao)
  const publis = await fetch(urlPublicacoes)
  const jsonLeis = await leis.json()
  const jsonPublis = await publis.json()
  
  var leisByParlamentarId = {}
  jsonLeis['data'].forEach(l=>{
    if(leisByParlamentarId[l.autoria]){
      leisByParlamentarId[l.autoria].push(l)
    }else{
      leisByParlamentarId[l.autoria] = []
      leisByParlamentarId[l.autoria].push(l)

    }
  })
  var publisByParlamentarId = {}
  jsonPublis['data'].forEach(l=>{
    if(publisByParlamentarId[l.autoria]){
      publisByParlamentarId[l.autoria].push(l)
    }else{
      publisByParlamentarId[l.autoria] = []
      publisByParlamentarId[l.autoria].push(l)

    }
  })
  
  const parlamentares = await fetch(urlParlamentares)
  const jsonParlamentares = await parlamentares.json()

  var parlamentaresOk = jsonParlamentares['data'].map(p=>{
    var fileData = ''
    if(filesById[p.photo]){
      fileData = filesById[p.photo].data.full_url
    }
    
    return {
      ...p,
      photo: fileData,
      legislacao: leisByParlamentarId[p.id],
      publicacoes: publisByParlamentarId[p.id]
    }
  })

  return parlamentaresOk
}

const getRegimeInterno = async () => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['arquivos_orgao']
  const arq = await fetch(url)
  const json = await arq.json()

  if(json.data.length === 0){
    return null
  }
  const regimeInternoId = json.data[0].regimento_interno
  const urlFiles = baseUrl+config.api.urls['media']+'&filter[id]='+regimeInternoId

  const files = await fetch(urlFiles)
  const jsonFiles = await files.json()
  
  if(jsonFiles.data.length === 0){
    return null
  }

  return jsonFiles.data[0]
}
const getLeiOrganica = async () => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['arquivos_orgao']
  const arq = await fetch(url)
  const json = await arq.json()

  if(json.data.length === 0){
    return null
  }
  const leiOrganicaId = json.data[0].lei_organica
  const urlFiles = baseUrl+config.api.urls['media']+'&filter[id]='+leiOrganicaId

  const files = await fetch(urlFiles)
  const jsonFiles = await files.json()
  
  if(jsonFiles.data.length === 0){
    return null
  }

  return jsonFiles.data[0]
}

const getFormSicPF = async () => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['arquivos_orgao']
  const arq = await fetch(url)
  const json = await arq.json()

  if(json.data.length === 0){
    return null
  }
  const formId = json.data[0].formulario_sic_pf
  const urlFiles = baseUrl+config.api.urls['media']+'&filter[id]='+formId

  const files = await fetch(urlFiles)
  const jsonFiles = await files.json()
  
  if(jsonFiles.data.length === 0){
    return null
  }

  return jsonFiles.data[0]
}
const getFormSicPJ = async () => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['arquivos_orgao']
  const arq = await fetch(url)
  const json = await arq.json()

  if(json.data.length === 0){
    return null
  }
  const formId = json.data[0].formulario_sic_pj
  const urlFiles = baseUrl+config.api.urls['media']+'&filter[id]='+formId

  const files = await fetch(urlFiles)
  const jsonFiles = await files.json()
  
  if(jsonFiles.data.length === 0){
    return null
  }

  return jsonFiles.data[0]
}
const getLinkReceitas = async () => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['arquivos_orgao']
  const arq = await fetch(url)
  const json = await arq.json()

  if(json.data.length === 0){
    return null
  }
  return json.data[0].link_receitas
}
const getLinkDespesas = async () => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['arquivos_orgao']
  const arq = await fetch(url)
  const json = await arq.json()

  if(json.data.length === 0){
    return null
  }
  return json.data[0].link_despesas
}

const getSolicitacoesSic = async () => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['e-sic']
  const arq = await fetch(url)
  const json = await arq.json()
  if(json.data.length === 0){
    return []
  }
  return json.data
}

const getSobre = async () => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['arquivos_orgao']
  const arq = await fetch(url)
  const json = await arq.json()

  if(json.data.length === 0){
    return null
  }
  return json.data[0].sobre
}
const getFile = async (id) => {
  const baseUrl = config.api.urls.base

  const url = baseUrl+config.api.urls['media']+'&filter[id]='+id
  const arq = await fetch(url)
  const json = await arq.json()

  if(json.data.length === 0){
    return null
  }

  return json.data[0]
}

const search = async (value) => {
  const baseUrl = config.api.urls.base
  const urlsToSrc = config.api.urls
  const urls = Object.keys(urlsToSrc)
  
  var results = urls.map(async key=>{
    if(key !== 'base' && key !== 'users' && key !== 'media' && key !== 'arquivos_orgao'){
      var url = urlsToSrc[key]
      var full_url = baseUrl+url+'&q='+value

      var src = await fetch(full_url)
      var json = await src.json()
      var res = {}
      if(json.data.length > 0){
        res[key] = json.data
        return res
      }else{
        return null
      }
      
    }else{
      return null
    }
  })
  return Promise.all(results).then(r=>{
    return r.filter(v=>v!==null)
  })
}



  

  export {
    getParlamentares,
    getLeiOrganica,
    getRegimeInterno,
    getLinkDespesas,
    getLinkReceitas,
    getComissoes,
    getSolicitacoesSic,
    getFormSicPF,
    getFormSicPJ,
    search,
    getSobre,
    getFile
  }