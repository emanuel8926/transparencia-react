import config from '../config.json'
import moment from 'moment'
import striptags from 'striptags'
import {AllHtmlEntities} from 'html-entities'

const getCategorieById = (id,data) => {
    return data.filter(v=>{
        return v.id == id
    })
}
const getTagById = (id,data) => {
    const filtred = data.filter(v=>{
        return v.id == id
    })
    if(filtred.length > 0){
        return filtred[0]
    }else{
        return {}
    }
}
const getAuthorById = (id,data) => {
    const filtred = data.filter(v=>{
        return v.id == id
    })
    if(filtred.length > 0){
        return filtred[0]
    }else{
        return {}
    }
}
const getMediaById = (id,data) => {
    const filtred = data.filter(v=>{
        return v.id == id
    })

    if(filtred.length > 0){
        return filtred[0].guid.rendered
    }else{
        return {}
    }
}
const getMediaByIdDirectus = (id,data) => {
    const filtred = data.filter(v=>{
        return v.id == id
    })

    if(filtred.length > 0){
        return filtred[0].data.full_url
    }else{
        return {}
    }
}
const getNestedValue = (path,data) => {
    const splited = path.split('.')
    try{
        var value = data
        splited.forEach(index=>{
            value = value[index]
        })
        return value
    }catch(err){
        return null
    }
}
const getDescription = (prop,values) => {
    const body = values[prop]
    return AllHtmlEntities.decode(striptags(body)).replace('Leia mais', '').slice(0,200)+'... '
}
const removeEmpty = (array) => {
    return array.filter(d=>{
        return d !== '' && d !== null
    })
}

const utils = {
    getAuthorById,
    getCategorieById,
    getMediaById,
    getMediaByIdDirectus,
    getNestedValue,
    getTagById,
    getDescription,
    removeEmpty
}

const proccessField = (fieldConfig, values, offData) => {
    switch(fieldConfig.type){
        case 'string': 
            if(fieldConfig.processor){
                return utils[fieldConfig.processor](fieldConfig.prop,values)
            }else{
                return values[fieldConfig.prop]
            }
        case 'date':
            return moment(values[fieldConfig.prop], fieldConfig.format).format(fieldConfig.displayFormat)
        case 'array':
            if(fieldConfig.offData){
                return values[fieldConfig.prop].map(v=>{
                    if(fieldConfig.processor){
                        return utils[fieldConfig.processor](v,offData)
                    }else{
                        return v
                    }
                })
            }else{
                if(fieldConfig.processor){
                    return utils[fieldConfig.processor](values[fieldConfig.prop])
                }else{
                    return values[fieldConfig.prop]
                }
            }
        case 'number':
            if(fieldConfig.processor){
                return utils[fieldConfig.processor](values[fieldConfig.prop], offData)
            }else{
                return values[fieldConfig.prop]
            }
    }
}
const preparePostsProps = (params) =>{
    const { posts } = params
    const postConfig = config.api.models.posts
    return posts.map(v=>{
        var newData = {}
        Object.keys(postConfig).forEach(field=>{
            var cfg = postConfig[field]
            newData[field] = proccessField(cfg,v,params[cfg.offData])
        })
        return newData
    })
}
const preparePostProps = (postData,params) =>{
    const postConfig = config.api.models.posts
    var newData = {}
    Object.keys(postConfig).forEach(field=>{
        var cfg = postConfig[field]
        newData[field] = proccessField(cfg,postData,params[cfg.offData])
    })
    return newData
}

export {
    preparePostProps,
    preparePostsProps,
    getMediaByIdDirectus
}
