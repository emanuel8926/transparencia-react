import fetch from 'isomorphic-unfetch'
import config from '../config.json'
import { getParlamentares } from './default'

  const getAll = async (slug) => {
    const baseUrl = config.api.urls.base

    const url = baseUrl+config.api.urls[slug]

    const data = await fetch(url)
    const jsonData = await data.json()

    return jsonData.data
  }

  const getDiarias = async () => {
    const baseUrl = config.api.urls.base
    const urlDiarias = baseUrl+config.api.urls['diarias']
    const diarias = await fetch(urlDiarias)
    const jsonDiarias = await diarias.json()

    const parlamentares = await getParlamentares()

    var parlamentaresById = {}
    parlamentares.forEach(p=>{
      parlamentaresById[p.id] = p
    })
    var diariasOk = jsonDiarias['data'].map(d=>{
      if(!parlamentaresById[d.id_parlamentar] ){
        return d;
      }
      return {
        ...d,
        id_parlamentar: parlamentaresById[d.id_parlamentar] ,
        responsavel: parlamentaresById[d.id_parlamentar].nome

      }
    })
    return diariasOk
  }

  export {
    getAll,
    getDiarias
  }