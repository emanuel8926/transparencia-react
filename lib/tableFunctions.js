import config from '../config.json'
import moment from 'moment'
import DownloadButton from '../components/downloadButton'
import DateColumn from '../components/dateColumn'
import StatusColumn from '../components/statusColumns'
import MoneyColmun from '../components/moneyColumn'
import ParlamentarColumn from '../components/parlamentarColumn'

const downloadColumn = (rowData, column) => {
    return <DownloadButton text="PDF" href={rowData.arquivo} fileId={rowData.file || 0} />;
}
const parlamentarColumn = (rowData, column) => {
    return <ParlamentarColumn rowData={rowData} parlamentarData={rowData.id_parlamentar || {}} />;
}
const dateColumn = (rowData, column) => {
    return <DateColumn value={rowData[column.field]} format={column.field}/>;
}
const statusColumn = (rowData, column) => {
    return <StatusColumn data={rowData}/>;
}
const moneyColumn = (rowData, column) => {
    return <MoneyColmun value={rowData[column.field]} />
}
const filterDate = (value,filter) =>{
    var val = moment(value).isValid()?moment(value).format('DD/MM/YYYY'):''
    return val.includes(filter)
  }

const sortCompetencia = (value,data) =>{
    data.sort((v1,v2)=>{
        
        var val2 = v2.competencia.split('/').reverse().join('')
        var val1 = v1.competencia.split('/').reverse().join('')
        return value.order * ( parseInt(val2) - parseInt(val1) )
    })
    return data
}
  
const filterParlamentar = (value,filter) =>{
    var val = moment(value).isValid()?moment(value).format('DD/MM/YYYY'):''
    return val.includes(filter)
}
const functions = {
    downloadColumn,
    dateColumn,
    filterDate,
    moneyColumn,
    parlamentarColumn,
    sortCompetencia,
    statusColumn
}
const mountTableColumns = (slug, data) => {
    var model = config.api.models[slug].columns
    return model.map(m=>{
        var body = null
        var filterFunction = null
        var sortFunction = null
        if(m.body){
            body = functions[m.body]
        }
        if(m.filterFunction){
            filterFunction = functions[m.filterFunction]
        }
        if(m.sortFunction){
            sortFunction = (val)=>{
                return functions[m.sortFunction](val,data)
            }
        }
        return {
            ...m,
            body,
            filterFunction,
            sortFunction
        }
    })

}
const getTableConfig = (slug, data=null) => {
    var cfg = Object.assign({},config.api.models[slug])
    cfg.columns = mountTableColumns(slug,data)
    return cfg
}

export {
    getTableConfig
}
