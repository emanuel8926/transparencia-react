CREATE DEFINER=`ext`@`%` TRIGGER TAddQueueEmail AFTER UPDATE
ON sic_mensagens_recebidas
FOR EACH ROW
BEGIN
	IF new.status_msg = 'respondida' and old.status_msg !=new.status_msg THEN
		insert into `email_queue` (`from`,`to`,`subject`,`html`) VALUES (concat((select nome_orgao from email_config limit 1), ' <',(select email_sender from email_config limit 1), '>'), new.email_solicitante, concat('[e-Sic] ','Sua solicitação de informação foi respondida!') , concat('<h4> Porta da Transparência </h4><p><b>Sua solicitação de informação ','<b> (Protocolo: ',new.protocolo,') </b> foi respondida!</b></p><br/><p><b>Resposta:</b></p><p>',new.resposta,'</p><br/>'));
	END IF;
    IF new.status_msg = 'indeferida' and old.status_msg !=new.status_msg THEN
		insert into `email_queue` (`from`,`to`,`subject`,`html`) VALUES (concat((select nome_orgao from email_config limit 1), ' <',(select email_sender from email_config limit 1), '>'), new.email_solicitante, concat('[e-Sic] ','Sua solicitação de informação foi INDEFERIDA!'), concat('<h4> Porta da Transparência </h4><p><b>Sua solicitação de informação ','<b> (Protocolo: ',new.protocolo,') </b> foi indeferida!</b></p><br/><br/>'));
	END IF;
    
END