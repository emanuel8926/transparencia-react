-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: transp_cam_jequia
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arquivos_orgao`
--

DROP TABLE IF EXISTS `arquivos_orgao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arquivos_orgao` (
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `lei_organica` int(10) unsigned DEFAULT NULL,
  `regimento_interno` int(10) unsigned DEFAULT NULL,
  `link_receitas` varchar(200) DEFAULT NULL,
  `link_despesas` varchar(200) DEFAULT NULL,
  `formulario_sic_pj` int(10) unsigned DEFAULT NULL,
  `formulario_sic_pf` int(10) unsigned DEFAULT NULL,
  `sobre` text,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comissoes`
--

DROP TABLE IF EXISTS `comissoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comissoes` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(20) DEFAULT 'draft',
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `presidente` int(10) unsigned DEFAULT NULL,
  `relator` int(10) unsigned DEFAULT NULL,
  `secretario` int(10) unsigned DEFAULT NULL,
  `secretario2` int(10) unsigned DEFAULT NULL,
  `vice_presidente` int(10) unsigned DEFAULT NULL,
  `membro` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email_config`
--

DROP TABLE IF EXISTS `email_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_config` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `nome_orgao` varchar(200) DEFAULT NULL,
  `email_sender` varchar(200) DEFAULT NULL,
  `email_sender_password` varchar(200) DEFAULT NULL,
  `email_enviar_msg_sic` varchar(200) DEFAULT NULL COMMENT 'Os emails devem ser separados por ; (ponto e vírgula)',
  `smtp_host` varchar(200) DEFAULT NULL,
  `smtp_port` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email_queue`
--

DROP TABLE IF EXISTS `email_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` varchar(300) DEFAULT NULL,
  `to` longtext,
  `subject` varchar(300) DEFAULT NULL,
  `html` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parlamentares`
--

DROP TABLE IF EXISTS `parlamentares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parlamentares` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `photo` int(10) unsigned DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `apelido` varchar(200) DEFAULT NULL,
  `naturalidade` varchar(200) DEFAULT NULL,
  `profissao` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `graducao` varchar(200) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `partido` varchar(200) DEFAULT NULL,
  `mandato` varchar(200) DEFAULT NULL,
  `cargo` varchar(100) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pautas`
--

DROP TABLE IF EXISTS `pautas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pautas` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `data_sessao` date DEFAULT NULL,
  `arquivo` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(20) DEFAULT 'draft',
  `sort` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL COMMENT 'Título da postagem',
  `tags` varchar(2000) DEFAULT NULL COMMENT 'Insira as tags da postagem e tecle enter',
  `slug` varchar(200) DEFAULT NULL,
  `post_body` longtext,
  `featured_image` int(11) DEFAULT NULL COMMENT 'Insira a imagem de destaque da postagem',
  `image_1` int(10) unsigned DEFAULT NULL,
  `image_2` int(10) unsigned DEFAULT NULL,
  `image_3` int(10) unsigned DEFAULT NULL,
  `image_4` int(10) unsigned DEFAULT NULL,
  `image_5` int(10) unsigned DEFAULT NULL,
  `image_6` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `servidores_queue`
--

DROP TABLE IF EXISTS `servidores_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servidores_queue` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `dados` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sic_mensagens_recebidas`
--

DROP TABLE IF EXISTS `sic_mensagens_recebidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sic_mensagens_recebidas` (
  `id` int(15) unsigned NOT NULL AUTO_INCREMENT,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_on` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_msg` varchar(20) DEFAULT 'aguardando',
  `protocolo` varchar(100) DEFAULT NULL COMMENT 'Protocolo da solicitação',
  `nome_solicitante` varchar(200) DEFAULT NULL COMMENT 'Node de quem solicitou a informação...',
  `email_solicitante` varchar(200) DEFAULT NULL,
  `area_solicitacao` varchar(200) DEFAULT NULL,
  `assunto` varchar(200) DEFAULT NULL,
  `mensagem` text,
  `resposta` text,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `protocolo` (`protocolo`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`ext`@`%`*/ /*!50003 TRIGGER TAddQueueEmail AFTER UPDATE
ON sic_mensagens_recebidas
FOR EACH ROW
BEGIN
	IF new.status_msg = 'respondida' and old.status_msg !=new.status_msg THEN
		insert into `email_queue` (`from`,`to`,`subject`,`html`) VALUES (concat((select nome_orgao from email_config limit 1), ' <',(select email_sender from email_config limit 1), '>'), new.email_solicitante, concat('[e-Sic] ','Sua solicitação de informação foi respondida!') , concat('<h4> Porta da Transparência </h4><p><b>Sua solicitação de informação ','<b> (Protocolo: ',new.protocolo,') </b> foi respondida!</b></p><br/><p><b>Resposta:</b></p><p>',new.resposta,'</p><br/>'));
	END IF;
    IF new.status_msg = 'indeferida' and old.status_msg !=new.status_msg THEN
		insert into `email_queue` (`from`,`to`,`subject`,`html`) VALUES (concat((select nome_orgao from email_config limit 1), ' <',(select email_sender from email_config limit 1), '>'), new.email_solicitante, concat('[e-Sic] ','Sua solicitação de informação foi INDEFERIDA!'), concat('<h4> Porta da Transparência </h4><p><b>Sua solicitação de informação ','<b> (Protocolo: ',new.protocolo,') </b> foi indeferida!</b></p><br/><br/>'));
	END IF;
    
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `transp_atas`
--

DROP TABLE IF EXISTS `transp_atas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_atas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(1000) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_balancetes`
--

DROP TABLE IF EXISTS `transp_balancetes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_balancetes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mes` varchar(45) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL COMMENT 'Arquivo anexo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_contratos`
--

DROP TABLE IF EXISTS `transp_contratos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_contratos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(200) DEFAULT NULL,
  `objeto` varchar(1000) DEFAULT NULL,
  `modalidade` varchar(200) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `vigencia` varchar(45) DEFAULT NULL,
  `credor` varchar(145) DEFAULT NULL,
  `valor` decimal(20,2) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_convenios`
--

DROP TABLE IF EXISTS `transp_convenios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_convenios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(45) DEFAULT NULL,
  `objeto` varchar(1000) DEFAULT NULL,
  `concedente` varchar(145) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `vigencia` varchar(45) DEFAULT NULL,
  `valor` decimal(20,2) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_diarias`
--

DROP TABLE IF EXISTS `transp_diarias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_diarias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `responsavel` varchar(245) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `motivo` varchar(145) DEFAULT NULL,
  `destino` varchar(145) DEFAULT NULL,
  `valor` decimal(20,2) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `id_parlamentar` int(10) unsigned DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_duodecimos`
--

DROP TABLE IF EXISTS `transp_duodecimos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_duodecimos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mes` varchar(45) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `valor` decimal(20,2) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_ldo`
--

DROP TABLE IF EXISTS `transp_ldo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_ldo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lei` varchar(45) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `data_publicacao` date DEFAULT NULL,
  `vigencia` varchar(100) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_legislacao`
--

DROP TABLE IF EXISTS `transp_legislacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_legislacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(45) DEFAULT NULL COMMENT '''''',
  `descricao` longtext COMMENT '''''',
  `tipo` varchar(100) DEFAULT NULL COMMENT '''''',
  `ano` year(4) DEFAULT NULL COMMENT '''''',
  `vigencia` varchar(100) DEFAULT NULL COMMENT '''''',
  `arquivo` longtext COMMENT '''''',
  `file` int(10) unsigned DEFAULT NULL,
  `autoria` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_licitacoes`
--

DROP TABLE IF EXISTS `transp_licitacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_licitacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(45) DEFAULT NULL,
  `objeto` longtext,
  `modalidade` varchar(100) DEFAULT NULL,
  `data_realizacao` date DEFAULT NULL,
  `situacao` varchar(45) DEFAULT NULL,
  `encerramento` date DEFAULT NULL,
  `vencedor` varchar(145) DEFAULT NULL,
  `cnpj_vencedor` varchar(45) DEFAULT NULL,
  `arquivo` longtext,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_loa`
--

DROP TABLE IF EXISTS `transp_loa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_loa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lei` varchar(45) DEFAULT NULL,
  `ano` varchar(4) DEFAULT NULL,
  `data_publicacao` date DEFAULT NULL,
  `vigencia` varchar(100) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_patrimonio`
--

DROP TABLE IF EXISTS `transp_patrimonio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_patrimonio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(45) DEFAULT NULL,
  `descricao` varchar(1000) DEFAULT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  `data_aquisicao` date DEFAULT NULL,
  `valor` decimal(20,2) DEFAULT NULL,
  `arquivo` longtext,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_ppa`
--

DROP TABLE IF EXISTS `transp_ppa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_ppa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lei` varchar(45) DEFAULT NULL,
  `ano` varchar(4) DEFAULT NULL,
  `data_publicacao` date DEFAULT NULL,
  `vigencia` varchar(100) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_prest`
--

DROP TABLE IF EXISTS `transp_prest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_prest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(45) DEFAULT NULL,
  `ano` varchar(4) DEFAULT NULL,
  `periodo` varchar(100) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_publicacoes`
--

DROP TABLE IF EXISTS `transp_publicacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_publicacoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(1000) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_qdd`
--

DROP TABLE IF EXISTS `transp_qdd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_qdd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ano` int(11) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_reembolsos`
--

DROP TABLE IF EXISTS `transp_reembolsos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_reembolsos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `exercicio` int(11) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  `valor` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_rgf`
--

DROP TABLE IF EXISTS `transp_rgf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_rgf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ano` int(11) DEFAULT NULL,
  `periodo` varchar(45) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_rreo`
--

DROP TABLE IF EXISTS `transp_rreo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_rreo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ano` int(11) DEFAULT NULL,
  `periodo` varchar(45) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_servidores`
--

DROP TABLE IF EXISTS `transp_servidores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_servidores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competencia` varchar(45) DEFAULT NULL,
  `dept` varchar(45) DEFAULT NULL,
  `mat` int(11) DEFAULT NULL,
  `nome` varchar(145) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `salario` decimal(20,2) DEFAULT NULL,
  `out_proventos` decimal(20,2) DEFAULT NULL,
  `sal_fam` decimal(20,2) DEFAULT NULL,
  `inss` decimal(20,2) DEFAULT NULL,
  `irrf` decimal(20,2) DEFAULT NULL,
  `out_descontos` decimal(20,2) DEFAULT NULL,
  `liquido` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=831 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transp_servidores_resumo`
--

DROP TABLE IF EXISTS `transp_servidores_resumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transp_servidores_resumo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competencia` varchar(45) DEFAULT NULL,
  `descricao` varchar(1000) DEFAULT NULL,
  `arquivo` varchar(1000) DEFAULT NULL,
  `file` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'transp_cam_jequia'
--

--
-- Dumping routines for database 'transp_cam_jequia'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-16  1:40:41
