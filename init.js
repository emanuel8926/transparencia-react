const fs = require('fs')
const https = require('https');

const configPath = process.argv[2]

if(!configPath){
  throw new Error("Arquivo de configuração não importado.")
}

const configData = JSON.parse( fs.readFileSync( configPath ).toString() )

fs.writeFileSync( './config.json', JSON.stringify( configData.siteconfig ) )
fs.writeFileSync( './server/lib/config.json', JSON.stringify( configData.dbconfig ) )
fs.writeFileSync( './styles/custom.sass', configData.sassconfig )
fs.writeFileSync( './process.json', JSON.stringify( configData.processconfig ) )


function saveImageToDisk(url, localPath) {
  var file = fs.createWriteStream(localPath);
  https.get(url, (response) => {
    response.pipe(file);
  });
}

saveImageToDisk( configData.bandeira, './static/img/bandeira.png' )


/*
if (!fs.existsSync( './config.json' )) {
  fs.copyFileSync( './config.sample.json', './config.json' )
}
if (!fs.existsSync( './server/lib/config.json' )) {
  fs.copyFileSync( './server/lib/config.sample.json', './server/lib/config.json' )
}
if (!fs.existsSync( './styles/custom.sass' )) {
  fs.copyFileSync( './styles/custom.example.sass', './styles/custom.sass' )
}
if (!fs.existsSync( './static/img/bandeira.png' )) {
  fs.copyFileSync( './static/img/sample.bandeira.png', './static/img/bandeira.png' )
}*/