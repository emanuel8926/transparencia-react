const withSass = require('@zeit/next-sass')
const withCSS = require('@zeit/next-css')
const webpack = require('webpack')

module.exports = {
  webpack: (config, { dev }) => {
    config.plugins.push(
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
        })
    )
    return config
  },
  target: 'server',
  ...withSass(withCSS({
    cssLoaderOptions: {
      url: false
    }
  }))
}