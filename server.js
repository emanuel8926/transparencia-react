
const next = require('next')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const server = express()

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
const port = process.env.PORT || 3000


server.use(cors());
server.use(bodyParser.json({limit: '150mb'}));
server.use(bodyParser.urlencoded({limit: '150mb', extended: false}));


app.prepare()
.then(() => {
  require('./server/routes')(server,app)
  
  server.get('*', (req, res) => {
    return handle(req, res)
  })
    
  server.listen(port, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:'+port )
  })
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})