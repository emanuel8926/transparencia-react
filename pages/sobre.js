
import config from '../config.json'
import React, { useState, useEffect } from 'react'
import PageHero from '../components/pageHero'
import { getSobre } from '../lib/default'
import SocialShare from '../components/socialShare'
import Loading from '../components/loading'

export default function () {
  const [cfg, setCfg] = useState(config.api.models['sobre'])
  const [sobre, setSobre] = useState(null)
  const [url, setUrl] = useState('')
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    if(!loaded){
      getSobre().then(r=>{
        if(r){
          setSobre(r)
        }
        setLoaded(true)
        setUrl(window.location.href)
      })
    }
  },[])
  return (
    <div >
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
        {
          !loaded?
          <Loading loading={true} />
          :
          <div class="content" style={{textAlign:'justify'}}  dangerouslySetInnerHTML={{__html:sobre}}>


          </div>

        }
      <br/>
          <br/>
          <br/>
    </div>
  )
}
