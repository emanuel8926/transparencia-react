
import config from '../config.json'
import React, { useState, useEffect } from 'react'
import PageHero from '../components/pageHero'
import { getRegimeInterno } from '../lib/default'
import SocialShare from '../components/socialShare'
import Loading from '../components/loading'

function RegimentoInterno () {
  const [cfg, setCfg] = useState(config.api.models['regimento-interno'])
  const [pdfUrl, setPdfUrl] = useState(null)
  const [url, setUrl] = useState('')
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    if(!loaded){
      getRegimeInterno().then(r=>{
        if(r){
          setPdfUrl(r.data.full_url)
        }
        setLoaded(true)
        setUrl(window.location.href)
      })
    }
  },[])
  return (
    <div >
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
        {
          !loaded?
          <Loading loading={true} />
          :
          <iframe width="100%" style={{height:"1220px"}} src={pdfUrl}/>

        }
      
    </div>
  )
}
export default RegimentoInterno