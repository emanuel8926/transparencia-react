
import config from '../config.json'
import Pautas from '../components/pautas'
import React, { useState, useEffect } from 'react'
import PageHero from '../components/pageHero'
import SocialShare from '../components/socialShare'

export default function() {
  const [cfg, setCfg] = useState(config.api.models['pautas'])
  const [url, setUrl] = useState('')
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    if(loaded){
      setUrl(window.location.href)
    }
  },[])
  return (
    <div >
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
      <Pautas onLoaded={()=>{setLoaded(true)}} table={false} />
    </div>
  )
}
