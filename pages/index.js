
import Posts from '../components/posts'
import config from '../config.json'
import Publicacoes from '../components/publicacoes'
import React, { useState, useEffect } from 'react'
import { getLinkDespesas, getLinkReceitas, getRegimeInterno,getLeiOrganica } from '../lib/default'
import Link from 'next/link'
import EsicAds from '../components/esicAds'
import Avisos from '../components/avisos'

function Page () {
  const dadosSessao = config.siteMeta.sessoes
  const [loaded, setLoaded] = useState(false)
  const [leiOrganica, setLeiOrg] = useState('#')
  const [regInt, setRegInt] = useState('#')

  /*useEffect(()=>{
    if(!loaded){
      getLeiOrganica().then(l=>{
        if(l.data){
          if(l.data.full_url){
            setLeiOrg(l.data.full_url)
          }
        }
        return getRegimeInterno().then(r=>{
          if(r.data){
            if(r.data.full_url){
              setRegInt(r.data.full_url)
              setLoaded(true)
            }
          }
          
        })
      })
      
    }
  })*/
  const titleComponent = ({title,subtitle,rightComponent}) => {
    return (
      <div class="columns">
        <div class="column ">
          <strong class="title">
            {title}
          </strong><br/>
          <small class="subtitle" style={{color:'grey'}}>
            {subtitle}
          </small>
        </div>
        <div class="column is-narrow ">
          {rightComponent}
        </div>
      </div>
    )
  }
  return (
    <div >
      
      <section class="section">
        <EsicAds/>
      </section>
      <section class="section " style={{wordWrap:'break-word'}}>
        <Avisos config={config}/>
      </section>
      <hr/>
      <section class="section  ">
        {titleComponent({
          title:"Últimas Notícias",
          subtitle:"Veja os últimos acontecimentos da casa legislativa.",
          rightComponent:(
            <Link href={"/noticias"}> 
              <a class="button is-primary is-outlined">Ver todas as Notícias </a> 
            </Link> 
          )
          })}
          <hr/>
        <Posts postsCount={3} onLoaded={()=>{setLoaded(true)}}/> 
      </section>
      <hr/>
      <section class="section" >
        <div class=" " >
          {titleComponent({
            title:"Últimas Publicações",
            subtitle:"Acompanhe a atividade da câmara.",
            rightComponent:(
              <Link href={"/transparencia/publicacoes"}> 
                <a class="button is-primary is-outlined">Ver todas as Publicações </a> 
              </Link> 
            )
            })}
          <hr/>
          <Publicacoes pubCount={10}/>
           
        </div>
      </section>
      <hr/>
      <section class="section" >
        <div class="columns notification is-danger">
          <div class="column  is-narrow">
            <span class="icon is-large">
              <i class="fas fa-3x fa-calendar-check"></i>
            </span>
          </div>
          <div class="column">
            <p>
              <b  style={{fontSize:14,color:'lightgrey'}}>Dias de Sessão:</b><br/>
              <strong class="has-text-white" style={{fontSize:20}}> {dadosSessao.dias} às {dadosSessao.horario}h.</strong>
              <hr/>
              &nbsp;<Link href="/pautas"><a  class="button is-warning is-outlined">Veja as pautas</a></Link>
            </p>
          </div>
        </div>
        <div class="columns"  style={{marginTop: '10px'}}>
          <div class="column" style={{marginRight: '10px'}}>
            <div class="columns notification is-link">
              <div class="column  is-narrow">
                <span class="icon is-large">
                  <i class="fas fa-3x fa-file"></i>
                </span>
              </div>
              <div class="column">
                <p>
                  <b  style={{fontSize:14,color:'lightgrey'}}>Acessar:</b><br/>
                  <strong class="has-text-white" style={{fontSize:20}}> Lei Orgânica</strong>
                  <hr/>
                  &nbsp;<Link href="/lei-organica"><a  class="button is-white is-outlined">Visualizar</a></Link>
                </p>
              </div>
            </div>
            
          </div>
          <div class="column">
            <div class="columns notification is-link">
              <div class="column  is-narrow">
                <span class="icon is-large">
                  <i class="fas fa-3x fa-file"></i>
                </span>
              </div>
              <div class="column">
                <p>
                  <b  style={{fontSize:14,color:'lightgrey'}}>Acessar:</b><br/>
                  <strong class="has-text-white" style={{fontSize:20}}> Regimento Interno</strong>
                  <hr/>
                  &nbsp;<Link href="/regimento-interno"><a  class="button is-white is-outlined">Visualizar</a></Link>
                </p>
              </div>
            </div>
          </div>
        </div>
        
      </section>
    </div>
  )
}
export default Page