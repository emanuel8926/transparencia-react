
import React, { useState, useEffect } from 'react'
import { search } from '../lib/default'
import config from '../config.json'
import { useRouter } from 'next/router';
import Loading from '../components/loading';
import MainTable from '../components/mainTable'
import PageHero from '../components/pageHero'
import SocialShare from '../components/socialShare'
import Link from 'next/link'
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import { getTableConfig } from '../lib/tableFunctions'

function Busca () {
  const router = useRouter();
  const { value } = router.query;
  const [loaded, setLoaded] = useState(false)
  const [url, setUrl] = useState('')
  const [result, setResult] = useState([])

  useEffect(()=>{
    if(!loaded){
      search(value).then(r=>{
        setResult(r)
        setUrl(window.location.href)
        setLoaded(true)
      })

      
    }
  },[])
  if(!loaded){
    return <Loading loading={true} />
  }

  return (
    <div class="container">
      <PageHero {...{
          title: `Buscando por: ${value}`,
          subtitle: "Resultados da busca",
          icon: "fas fa-3x fa-search",
          breadcrumb: [
            {"title": "Home","link":"/"},
            {"title": "Busca: "+value,"active":true}
        ],
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
        {
        //JSON.stringify(Object.keys(result))
        }
        {
          result.map(obj=>{
            var key = Object.keys(obj)[0]
            var values = obj[key]
            if(key!=='posts'){
              var cfg = getTableConfig(key,values)
              var urlpg = config.siteMeta.urls[key]
              var txt = <h2 class="subtitle" style={{fontWeight:'bold'}}>{cfg.title}</h2>
              return (
                <div>
                  {txt}
                  <hr/>
                  {
                    urlpg?
                    <Link href={urlpg}>Acessar página</Link>
                    :''
                  }
                  
                  <br/>
                  <DataTable 
                      value={values} 
                      paginator={true} 
                      rows={10} 
                      emptyMessage="Nenhum registro encontrado..." 
                  >
                      {
                          cfg['columns'].map((c,i)=>{
                              return (<Column key={i} {...c} />)
                          })
                      }
                  </DataTable>
                  {/*
                    cfg['columns']?
                    <table class="table">
                      <thead>
                        <tr>
                        {
                          cfg['columns'].map(c=>{
                            return (<th>{c.header}</th>)
                          })
                        }
                        </tr>
                      </thead>
                        <tbody>
                          {
                            values.map(v=>{
                              return <tr>
                                {
                                  cfg['columns'].map(c=>{
                                    return (<td>{v[c.field]}</td>)
                                  })
                                }

                              </tr>
                            })
                          }
                        </tbody>
                    </table>
                    
                    :
                    <Link href={url}><a class="button is-primary is-outlined">Ir para a página</a></Link>
                  */}
                  
                  <br/>
                  <br/>
                </div>
              )
              return (
                <MainTable config={cfg} data={values} />
              )
            }
          })
        }
      
    </div>
  )
}
export default Busca