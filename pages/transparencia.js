
import Posts from '../components/posts'
import Publicacoes from '../components/publicacoes'
import React, { useState, useEffect } from 'react'
import { getLinkDespesas, getLinkReceitas, getRegimeInterno,getLeiOrganica } from '../lib/default'
import Link from 'next/link'
import Loading from '../components/loading';
import PageHero from '../components/pageHero'
import SocialShare from '../components/socialShare'
import EsicAds from '../components/esicAds'
import config from '../config.json'

function Transparencia () {
  const [url, setUrl] = useState('')
  const [cfg, setCfg] = useState(config.api.models['transparencia'])
  const [loaded, setLoaded] = useState(false)
  const [leiOrganica, setLeiOrg] = useState('#')
  const [regInt, setRegInt] = useState('#')

  const tbs = [
    'licitacoes',
    'contratos',
    'diarias',
    'reembolsos',
    'receitas',
    'despesas',
    'duodecimos',
    'servidores',
    'plano-plurianual',
    'lei-de-diretrizes-orcamentarias',
    'lei-orcamentaria-anual',
    'quadro-de-detalhamento-da-despesa',    
    'relatorio-de-gestao-fiscal',
    'relatorio-resumido-da-execucao-orcamentaria',
    'balancetes',
    'prestacao-de-contas',
    'publicacoes',
    'atas-das-sessoes',
    'patrimonio',
    'estrutura-organizacional'
  ]

  useEffect(()=>{
    if(!loaded){
      setUrl(window.location.href)

      getLeiOrganica().then(l=>{
        if(l.data){
          if(l.data.full_url){
            setLeiOrg(l.data.full_url)
          }
        }
        return getRegimeInterno().then(r=>{
          if(r.data){
            if(r.data.full_url){
              setRegInt(r.data.full_url)
              setLoaded(true)
            }
          }
          
        })
      })
      
    }
  },[])
  const buttonComponent = (title,icon,link) => (
    <Link href={link}>
      <div class="card " style={{cursor:'pointer',height:'150px'}} >
        <div class="card-content">
          <div class="has-text-centered">
              <span class="icon has-text-primary is-large">
                  <i class={icon}></i>
              </span>
          </div>
          <div class="has-text-centered">
            <b class="titlePostGeneric">{title}</b> 

          </div>
        </div>
      </div>
    </Link>
  )
  return (
    <div >
      
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
        <EsicAds/>
        <div class="columns  is-multiline">
          {tbs.map(t=>{
            
            var cfg = config.api.models[t]
            return (
              <div class="column is-3">
                {buttonComponent(cfg.title,cfg.icon, "/transparencia/"+cfg.slug)}
              </div>
            )


          })}
        </div>
     
    </div>
  )
}
export default Transparencia