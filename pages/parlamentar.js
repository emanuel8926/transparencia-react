import React, { useState, useEffect} from 'react'
import { useRouter } from 'next/router';
import { getParlamentares } from '../lib/default'
import PageHero from '../components/pageHero'
import SocialShare from '../components/socialShare'
import config from '../config.json'
import Loading from '../components/loading'
import DownloadButton from '../components/downloadButton'
import moment from 'moment'


const Parlamentares = () => {  
  
  const router = useRouter();
  const { slug } = router.query;
  const [url, setUrl] = useState('')
  const [dados, setDados] = useState({})
  const [loaded, setLoaded] = useState(false)
  const [tabActive, setActiveTab] = useState('informacoes')
  const [legs, setLegs] = useState({})
  const [publis, setPublis] = useState({})
  const [photo, setPhoto] = useState([])

  useEffect(() => {
    if(!loaded){
      getParlamentares(slug).then(r=>{
        setUrl(window.location.href)
        if(r.length > 0){
          setDados(r[0])
          console.log(r[0])

          if(r[0].legislacao){
            if(r[0].legislacao.length > 0){
              let types = {}
              r[0].legislacao.forEach(l=>{
                if(types[l.tipo]){
                  types[l.tipo].push(l)
                }else{
                  types[l.tipo] = []
                  types[l.tipo].push(l)
                }
              })
              setLegs(types)
            }
          }
          if(r[0].publicacoes){
            if(r[0].publicacoes.length > 0){
              let types = {}
              r[0].publicacoes.forEach(l=>{
                if(types[l.tipo]){
                  types[l.tipo].push(l)
                }else{
                  types[l.tipo] = []
                  types[l.tipo].push(l)
                }
              })
              setPublis(types)
            }
          }
          setPhoto(r[0].photo)
        }
        setLoaded(true)
      })
    }
    
  },[])

  const setCargo = (cargo) => {
    switch(cargo){
        case 'presidente': return "Presidente";
        case 'vice_presidente': return "Vice-Presidente";
        case '1_secretario': return "1º Secretário";
        case '2_secretario': return "2º Secretário";
        case 'vereador': return "Vereador";
    }
  }
  if(!loaded){
    return <Loading loading={true} />
  }
  
  if(loaded && Object.keys(dados).length === 0){
    return "Parlamentar não encontrado..."
  }
  const textForTypes = {
    "Lei": "Leis de sua autoria",
    "Projetos de Lei": "Projetos de Lei",
    "Moção": "Moções",

  }
  return (
    <>
    <PageHero {...{
        title: dados.nome,
        subtitle: `${setCargo(dados.cargo)}`,
        icon: 'fas fa-3x fa-user',
        photo:dados.photo,
        breadcrumb: [
          {"title": "Home","link":"/"},
          {"title": "Parlamentares", "link": '/parlamentares'},
          {"title": dados.nome,"active":true}
      ],
        rightComponent:(
          <SocialShare url={url} />
        )
      }
    }/>
    
    <div class="tabs  ">
        <ul>
          <li class={tabActive==='informacoes'?"is-active":""} onClick={()=>setActiveTab('informacoes')}>      
            <a class="">
              <span>Informações</span>
            </a>

          </li>
          {
            Object.keys(legs || {}).concat(Object.keys(publis || {})).map((tipo,i)=>{
              return (
                <li class={tabActive===tipo?"is-active":""} onClick={()=>setActiveTab(tipo)}>
                
                  <a class="">
                    <span>{tipo}</span>
                  </a>

                </li>
              )
            })
          }
        </ul>
      </div>
      {
        tabActive === 'informacoes'?
        <div className="bodyPostGeneric" style={{height:'600px'}}>
          <b>Mandato: </b> {dados.mandato}<br/>
          <b>Partido: </b> {dados.partido}<br/>
          <b>Naturalidade: </b> {dados.naturalidade}<br/>
          <b>Profissão: </b> {dados.profissao}<br/>
          <b>Graduação: </b> {dados.graducao}<br/>
          <b>Nascimento: </b> {moment(dados.data_nascimento).format('DD/MM/YYYY')}<br/>
          <b>Email: </b> {dados.email}<br/>
        </div>
        :
        <div>
          {
            (legs[tabActive] || []).concat( (publis[tabActive] || []) ).map(v=>{
              return (
                <article class="media">
                  <div class="media-content">
                    <div class="content">
                      <p>
                        {
                          v.ano && v.vigencia?
                          <div>
                            <small><b>Ano: </b>{v.ano}</small> / &nbsp; 
                            <small><b>Vigência: </b>{v.vigencia}</small>
                          </div>
                          :
                          <div>
                            <small><b>Data: </b>{moment(v.data).format('DD/MM/YYYY')}</small>
                          </div>
                        }
                        {v.descricao}  
                      </p>
                    </div>
                  </div>
                  <div class="media-right">
                    <DownloadButton href={v.arquivo} fileId={v.file || 0} text="Download" />
                  </div>
                </article>
              )
            })
          }
        </div>
      }
    
    </>
  );
};
export default Parlamentares;