import React, { useState, useEffect} from 'react'
import PageHero from '../components/pageHero'
import SocialShare from '../components/socialShare'
import config from '../config.json'
import ParlamentaresComponent from '../components/parlamentares'


const Parlamentares = () => {  
  
  const pageData = config.api.models.parlamentares 
  const [loaded, setLoaded] = useState(false)
  const [url, setUrl] = useState('')

  useEffect(() => {
      setUrl(window.location.href)
  },[])
  return (
    <>
    <PageHero {...{
        title: pageData.title,
        subtitle: pageData.subtitle,
        icon: pageData.icon,
        breadcrumb: pageData.breadcrumb,
        rightComponent:(
          <SocialShare url={url} />
        )
      }
    }/>
      <ParlamentaresComponent onLoaded={()=>setLoaded(true)} />
    
    </>
  );
};
export default Parlamentares;