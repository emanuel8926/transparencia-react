
import config from '../../config.json'
import { getComissoes } from '../../lib/default'
import React, { useState, useEffect } from 'react'
import PageHero from '../../components/pageHero'
import { getTableConfig } from '../../lib/tableFunctions'
import SocialShare from '../../components/socialShare'
import Loading from '../../components/loading.js';
import Link from 'next/link'

function Page () {
  const [cfg, setCfg] = useState(config.api.models['estrutura-organizacional'])
  const [url, setUrl] = useState('')
  const [loaded, setLoaded] = useState(false)
  const [comissoes, setComissoes] = useState({})
  const [mesaDiretora, setMesa] = useState({})

  useEffect(() => {
    if(!loaded){
      getComissoes().then(r=>{
        var com = {}
        var mesaDiretora = {}
        r.forEach(c=>{
          if(c.titulo === 'Mesa Diretora'){
            mesaDiretora = c
          }else{
            com[c.titulo] = c
          }
        })
        setMesa(mesaDiretora)
        setComissoes(com)
        setUrl(window.location.href)
        setLoaded(true)
      })
    }
  },[])
  const cardParlamentar = (params, posicao) => {
    const {nome, partido, cargo, slug} = params
    return (
      <div class="card " >
        <div class="card-content">
          <article class="media">
            <div class="media-content">
              <div class="content">
                <p>
                  <div className="titlePostGeneric">
                    <strong className="titlePostGeneric">{nome}</strong> 
                  </div>
                  <div className="subTitlePostGeneric">
                    <p className="subTitlePostGeneric has-text-grey"><small>{posicao}</small></p>
                  </div>
                  
                  <Link href={'/parlamentares/'+slug}>
                    <a>Acessar página</a>
                  </Link>
                </p>
              </div>
              
            </div>
          </article>
        </div>
      </div>
    )
  }
  return (
    <div >
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
        <div class="has-text-left">
          {
            !loaded?
            <Loading loading={true} />
            :
            comissoes.length === 0?
            <h1>Nenhum resultado encontrado...</h1>
            :
            <div>
              <div>
                <h1 class="subtitle"><b>Mesa Diretora</b></h1>
                <section class="container">
                  <div class="columns is-multiline">                    
                  {
                      mesaDiretora.presidente?
                      <div class="column is-3" >
                        {cardParlamentar(mesaDiretora.presidente, "Presidente")}
                      </div>
                      :""
                    }
                    {
                      mesaDiretora.vice_presidente?
                      <div class="column is-3" >
                        {cardParlamentar(mesaDiretora.vice_presidente, "Vice-Presidente")}
                      </div>
                      :""
                    }
                    {
                      mesaDiretora.secretario?
                      <div class="column is-3" >
                        {cardParlamentar(mesaDiretora.secretario, "1º Secretário")}
                      </div>
                      :""
                    }
                    {
                      mesaDiretora.secretario2?
                      <div class="column is-3" >
                        {cardParlamentar(mesaDiretora.secretario2, "2º Secretário")}
                      </div>
                      :""
                    }
                    {
                      mesaDiretora.membro?
                      <div class="column is-3" >
                        {cardParlamentar(mesaDiretora.membro, "Membro")}
                      </div>
                      :""
                    }
                  </div>
                </section>
              </div>
              <hr/>

              <section class="container">
                <div >
              {
                Object.keys(comissoes).map(com=>{
                  var dt = comissoes[com]
                  return (
                    <>
                      <div>
                        <h1 class="subtitle"><b>Comissão: {com}</b></h1>
                        <section class="container">
                          <div class="columns is-multiline">
                            {
                              dt.presidente?
                              <div class="column is-3" >
                                {cardParlamentar(dt.presidente, "Presidente")}
                              </div>
                              :""
                            }

                            {
                              dt.vice_presidente?
                              <div class="column is-3" >
                                {cardParlamentar(dt.vice_presidente, "Vice-Presidente")}
                              </div>
                              :""
                            }
                            
                            {
                              dt.relator?
                              <div class="column is-3" >
                                {cardParlamentar(dt.relator, "Relator")}
                              </div>
                              :""
                            }
                            
                            {
                              dt.secretario?
                              <div class="column is-3" >
                                {cardParlamentar(dt.secretario, "1º Secretário")}
                              </div>
                              :""
                            }

{
                              dt.secretario2?
                              <div class="column is-3" >
                                {cardParlamentar(dt.secretario2, "2º Secretário")}
                              </div>
                              :""
                            }

                            {
                              dt.membro?
                              <div class="column is-3" >
                                {cardParlamentar(dt.membro, "Membro")}
                              </div>
                              :""
                            }

                          </div>
                        </section>
                      <hr/>
                      </div>
                    </>
                  )
                })
              }

                </div>
              </section>
            </div>
          }
        </div>
    </div>
  )
}
export default Page