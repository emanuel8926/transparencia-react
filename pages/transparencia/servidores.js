import BaseComponent from '../../components/baseComponent'
import { useRouter } from 'next/router';

export default () => {  
  const router = useRouter();
  const slug = router.pathname.split('/')[2];
  return (
    <>
      <BaseComponent slug={slug} haveModal={true}/>
    </>
  );
};