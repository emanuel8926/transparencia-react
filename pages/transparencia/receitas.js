
import config from '../../config.json'
import React, { useState, useEffect } from 'react'
import PageHero from '../../components/pageHero'
import { getLinkReceitas } from '../../lib/default'
import SocialShare from '../../components/socialShare'
import Loading from '../../components/loading'

function Receitas () {
  const [cfg, setCfg] = useState(config.api.models['receitas'])
  const [urlRec, setUrlRec] = useState(null)
  const [url, setUrl] = useState('')
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    if(!loaded){
      getLinkReceitas().then(r=>{
        console.log(r)
        if(r){
          setUrlRec(r)
        }
        setLoaded(true)
        setUrl(window.location.href)
      })
    }
  },[])
  return (
    <div >
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
        {
          !loaded?
          <Loading loading={true} />
          :
          <>
          <a href={urlRec} target="_blank">Clique para Acessar</a>
          <hr/>
          <iframe width="900px" style={{height:"1220px"}} src={urlRec}/>
          </>

        }
      
    </div>
  )
}
export default Receitas