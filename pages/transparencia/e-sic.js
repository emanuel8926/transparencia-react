
import config from '../../config.json'
import React, { useState, useEffect } from 'react'
import PageHero from '../../components/pageHero'
import SocialShare from '../../components/socialShare'
import Loading from '../../components/loading'
import FormSic from '../../components/formSic'
import { getSolicitacoesSic, getFormSicPF, getFormSicPJ } from '../../lib/default'
import MainTable from '../../components/mainTable'
import { getTableConfig } from '../../lib/tableFunctions'

function ESic () {
  const [cfg, setCfg] = useState(getTableConfig('e-sic'))
  const [url, setUrl] = useState('')
  const [loaded, setLoaded] = useState(false)
  const [solicitacoes, setSolicitacoes] = useState([])
  const [sicPf, setSicPf] = useState('')
  const [sicPj, setSicPj] = useState('')
  useEffect(() => {
    if(!loaded){
      getRegs()
    }
  }, [])
  const getRegs = () => {
    getSolicitacoesSic().then(r=>{
      setSolicitacoes(r)
      return getFormSicPF()
    }).then(f=>{
        if(f.data){
          if(f.data.full_url){
            setSicPf(f.data.full_url)
          }
        }
        return getFormSicPJ()
      }).then(j=>{
        if(j.data){
          if(j.data.full_url){
            setSicPj(j.data.full_url)
            setLoaded(true)
            setUrl(window.location.href)
          }
        }
      })
  }
  const groupSolicitacoes = (solicitacoes) => {
    if(!solicitacoes.length){
      return ''
    }
    const grouped = {}
    solicitacoes.forEach(s=>{
      if(grouped[s.status_msg]){
        groupe[ds.status_msg].push(s)
      }else{
        grouped[s.status_msg] = [s]
      }
    })
    let result = []
    return Object.keys(grouped).map(g=>{
      const dados = grouped[g]
      return <div>
        <h3  class="subtitle"><b>Total {g}:</b> {dados.length}</h3>
      </div>
    })

    
  }
  return (
    <div >
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
        <section>
          O Sistema Eletrônico do Serviço de Informações ao Cidadão (e-SIC) permite que qualquer pessoa, física ou jurídica, encaminhe pedidos de acesso à informação, e receba a resposta da solicitação realizada para a Casa Legislativa. Por meio do sistema, além de fazer o pedido, será possível acompanhar ONLINE, e receber a resposta da solicitação por e-mail, e consultar as respostas recebidas ONLINE. 
        </section>
        <hr/>
        {
          !loaded?
          <Loading loading={true} />
          :
          <section>
            <FormSic onClose={()=> getRegs() }/>
            <hr/>

            {
              solicitacoes.length === 0?
              <p>

              <b>Nenhuma solicitação encontrada...</b><br/>
              </p>
              :
              <>
                <b>Para consultar um protocolo, basta digitar o número no campo de BUSCA.</b>
                <MainTable  config={cfg} data={solicitacoes}/>
                
                <hr/>
                {groupSolicitacoes(solicitacoes)}
              </>
            }
          </section>

        }
        <hr/>
        <section>
          <h1 class="subtitle"><b>SIC - Serviço de Informação ao Cidadão (Presencial)</b></h1>
          <p>
          O Serviço de Informações ao Cidadão (SIC) é a unidade física existente em todos os órgãos e entidades do poder público, em local identificado e de fácil acesso, para atender o cidadão que deseja solicitar o acesso à informação pública. Os SICs têm como objetivos: 
Atender e orientar o público quanto ao acesso às informações; 
Conceder o acesso imediato à informação disponível; 
Informar sobre a tramitação de documentos nas suas respectivas unidades; 
Protocolizar documentos e requerimentos de acesso às informações; 
          </p>
          <br/>
          <strong>Baixe o formulário correspondente abaixo, preencha e leve até a câmara.</strong>
          <br/>
          <a target="_blank" class="button is-link " href={sicPj}>Pessoa Jurídica</a> &nbsp;
          <a target="_blank" class="button is-link " href={sicPf}>Pessoa Física</a>
        </section>
        <br/>
        <strong>Onde Solicitar?</strong>
        <br/>
        <br/>
        <p>{config.siteMeta.endereco.logradouro}, {config.siteMeta.endereco.numero},</p>
        <p>{config.siteMeta.endereco.cidade}, {config.siteMeta.endereco.estado}, </p>
        <p><b>CEP:</b> {config.siteMeta.endereco.CEP}</p>
        <p><b>Telefone:</b>  {config.siteMeta.endereco.telefone}</p>
        <br/>
        {
          config.siteMeta.funcionamento.map(h=>{
            return (
              <p><b>{h.dia}: </b> {h.horario}</p>
            )
          })
        }
    </div>
  )
}
export default ESic