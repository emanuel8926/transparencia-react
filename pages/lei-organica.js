
import config from '../config.json'
import React, { useState, useEffect } from 'react'
import PageHero from '../components/pageHero'
import { getLeiOrganica } from '../lib/default'
import SocialShare from '../components/socialShare'
import Loading from '../components/loading'

function LeiOrganica () {
  const [cfg, setCfg] = useState(config.api.models['lei-organica'])
  const [pdfUrl, setPdfUrl] = useState(null)
  const [url, setUrl] = useState('')
  const [loaded, setLoaded] = useState(false)
  useEffect(() => {
    if(!loaded){
      getLeiOrganica().then(r=>{
        console.log(r)
        if(r){
          setPdfUrl(r.data.full_url)
        }
        setLoaded(true)
        setUrl(window.location.href)
      })
    }
  },[])
  return (
    <div >
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
        {
          !loaded?
          <Loading loading={true} />
          :
          <iframe width="100%" style={{height:"1220px"}} src={pdfUrl}/>

        }
      
    </div>
  )
}
export default LeiOrganica