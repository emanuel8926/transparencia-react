import React from 'react'
import App from 'next/app'
import Layout from '../components/layout'
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import '../styles/comps.css'


export default class MyApp extends App {
  render () {
    const { Component, pageProps } = this.props
    return (
      <Layout>
        <div>
        <Component {...pageProps} />
        </div>
      </Layout>
    )
  }
}