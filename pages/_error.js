
import React from 'react'

export default class Error extends React.Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode }
  }
  loadError(errorCode){
    switch(errorCode){
      case 404: return (
        <>
          <strong style={{fontSize:'30px'}}>Página não encontrada!</strong>
          <h1>Verifique se o endereço está correto, ou use a busca.</h1>
        </>
      );
      default: return (
        <>
          <strong style={{fontSize:'30px'}}>Erro de carregamento!</strong>
          <h1>Por favor tente mais tarde.</h1>
        </>
      )
    }
  }
  render() {
    return (
      <>
        <section className="container has-text-centered">
          <p>
            {
              this.loadError(this.props.statusCode)
            }
          </p>
        </section>
      </>
    )
  }
}