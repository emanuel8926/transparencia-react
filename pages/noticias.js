
import config from '../config.json'
import Posts from '../components/posts'
import { getPosts } from '../lib/postsFetch'
import React, { useState, useEffect } from 'react'
import PageHero from '../components/pageHero'
import { getTableConfig } from '../lib/tableFunctions'
import SocialShare from '../components/socialShare'

function Page () {
  const [cfg, setCfg] = useState(config.api.models['noticias'])
  const [url, setUrl] = useState('')
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    if(loaded){
      setUrl(window.location.href)
    }
  },[])
  return (
    <div >
      <PageHero {...{
          title: cfg.title,
          subtitle: cfg.subtitle,
          icon: cfg.icon,
          breadcrumb: cfg.breadcrumb,
          rightComponent:(
            <SocialShare url={url} />
          )
        }
        }/>
      <Posts onLoaded={()=>{setLoaded(true)}} />
    </div>
  )
}
export default Page