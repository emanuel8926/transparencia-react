import { useRouter } from 'next/router';
import striptags from 'striptags'
import {AllHtmlEntities} from 'html-entities'
import { getPost } from '../lib/postsFetch'
import React, { useState, useEffect } from 'react'
import config from '../config.json'
import Link from 'next/link'
import { inspect } from 'util'
import ImageGallery from 'react-image-gallery';
import SocialShare from '../components/socialShare'
import { NextSeo } from 'next-seo'
import Breadcrumb from '../components/breadcrumb'
import Loading from '../components/loading'

const Post = () => {
  const router = useRouter();
  const { slug } = router.query;
  const [post, setPost] = useState([])
  const [loaded, setLoaded] = useState(false)
  const [url, setUrl] = useState('')

  useEffect(() => {
    if(!loaded){
      getPost(config.api.provider, slug).then(r=>{
        //console.log(r)
        setPost(r)
        setUrl(window.location.href)
        setLoaded(true)
      })
    }    
  },[])
  if(!loaded){
    return (
      <>
        <Loading loading={loaded} text={" Notícia"}/>
      </>
    )
  }
  if(post.length === 0){
    return (
      <>
        <h1>Notícia não encontrada...</h1>
      </>
    )
  }
  const {
    ID,
    TITLE,
    DATE,
    SLUG,
    DESCRIPTION,
    BODY,
    AUTHOR,
    IMAGE,
    TAGS
  } = post[0]
  const [image1,image2,image3,image4,image5,image6] = [post[0]['IMAGE1'],post[0]['IMAGE2'],post[0]['IMAGE3'],post[0]['IMAGE4'],post[0]['IMAGE5'],post[0]['IMAGE6']]
  const images = [image1,image2,image3,image4,image5,image6].filter(v=>Object.keys(v).length > 0)
  return (
    <>
    
    
      <NextSeo
          title={AllHtmlEntities.decode(striptags(TITLE))+' - '+config.siteMeta.title}
          description={config.siteMeta.subtitle}
      />
      <Breadcrumb links={[
        {title:"Home", link:'/'},
        {title:"Notícias", link:'/noticias'},
        {title:AllHtmlEntities.decode(striptags(TITLE)), active:true}
      ]} />
      <div class="card">
        <div class="card-header">
          <div class="card-header-title">
            <div class="media">
              <div class="media-content">
                <p class="title is-4"><strong>{AllHtmlEntities.decode(striptags(TITLE))}</strong> </p>
                <p class="subtitle is-6"><small>{DATE}</small> - <small>{AUTHOR.first_name}</small></p>
              </div>
            </div>
          </div>
          
        </div>
        <div class="card-image">
          <figure class="imagePostFeat">
            <img src={IMAGE}  class="imagePostFeat"/>
          </figure>
        </div>
        <div class="card-content">
        <div class="tags" >
          {
            TAGS.map(t=>{
              return (
                <span class="tag is-link">{t}</span>
              )
            })
          }
        </div>
        <div class="socialShare">
          <SocialShare url={url} iconsSize={40}/>
        </div>

        <br/>
          <div class="content" dangerouslySetInnerHTML={{__html:BODY}}>
          </div>
          <br/>
          {
            images.length > 0?
            <div>
              <ImageGallery 
              showThumbnails={true} 
              showBullets={true} 
              showFullscreenButton={true} 
              showNav={true} 
              items={images.map(img=>{
                return {
                  original: img,
                  thumbnail: img,
                }
              })} />
            </div>
            :''
          }
          
        </div>
      </div>
    </>
  );
};
export default Post;